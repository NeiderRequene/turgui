-- Database generated with pgModeler (PostgreSQL Database Modeler).
-- pgModeler  version: 0.9.1
-- PostgreSQL version: 10.0
-- Project Site: pgmodeler.io
-- Model Author: ---


-- Database creation must be done outside a multicommand file.
-- These commands were put in this file only as a convenience.
-- -- object: new_database | type: DATABASE --
-- -- DROP DATABASE IF EXISTS new_database;
-- CREATE DATABASE new_database;
-- -- ddl-end --
-- 

-- object: public.lug_lugar | type: TABLE --
-- DROP TABLE IF EXISTS public.lug_lugar CASCADE;
CREATE TABLE public.lug_lugar(
	id_lugar serial NOT NULL,
	nombre varchar(500) NOT NULL,
	direccion varchar(100),
	referencia varchar(100),
	descripcion text,
	descripcion_corta varchar(250),
	latitud varchar(30) NOT NULL,
	longitud varchar(30) NOT NULL,
	codigo_postal varchar(20),
	prioridad integer,
	fecha_creacion timestamp,
	fecha_modificacion timestamp,
	id_usuario integer NOT NULL,
	id_tipo_lugar integer NOT NULL,
	id_division_politica integer NOT NULL,
	CONSTRAINT lugar_pk PRIMARY KEY (id_lugar)

);
-- ddl-end --
COMMENT ON TABLE public.lug_lugar IS 'Esta tabla contiene los datos de cada lugar que se puede buscar por la divisionPolitica';
-- ddl-end --
COMMENT ON COLUMN public.lug_lugar.id_lugar IS 'Identificador del lugar';
-- ddl-end --
COMMENT ON COLUMN public.lug_lugar.nombre IS 'Nombre del lugar';
-- ddl-end --
COMMENT ON COLUMN public.lug_lugar.direccion IS 'Dirección del lugar';
-- ddl-end --
COMMENT ON COLUMN public.lug_lugar.referencia IS 'Señal de referencia para ubicar el lugar';
-- ddl-end --
COMMENT ON COLUMN public.lug_lugar.descripcion IS 'Descripcion del lugar';
-- ddl-end --
COMMENT ON COLUMN public.lug_lugar.descripcion_corta IS 'Campo para registrar una descripcion del lugar en un máximo de 250 caracteres.';
-- ddl-end --
COMMENT ON COLUMN public.lug_lugar.latitud IS 'Latitud del lugar';
-- ddl-end --
COMMENT ON COLUMN public.lug_lugar.longitud IS 'Longitud del lugar';
-- ddl-end --
COMMENT ON COLUMN public.lug_lugar.codigo_postal IS 'Campo para registrar el código postral de un lugar.';
-- ddl-end --
COMMENT ON COLUMN public.lug_lugar.prioridad IS 'Campo para registrar la prioridad de un lugar.';
-- ddl-end --
COMMENT ON COLUMN public.lug_lugar.fecha_creacion IS 'Campo para registrar la fecha de creación de un registro';
-- ddl-end --
COMMENT ON COLUMN public.lug_lugar.fecha_modificacion IS 'Campo para registrar la fecha de modificacion de un registro';
-- ddl-end --
ALTER TABLE public.lug_lugar OWNER TO postgres;
-- ddl-end --

-- object: public.lug_tipo_lugar | type: TABLE --
-- DROP TABLE IF EXISTS public.lug_tipo_lugar CASCADE;
CREATE TABLE public.lug_tipo_lugar(
	id_tipo_lugar serial NOT NULL,
	nombre varchar(50) NOT NULL,
	descripcion varchar(100) NOT NULL,
	CONSTRAINT "tipoLugar_pk" PRIMARY KEY (id_tipo_lugar)

);
-- ddl-end --
COMMENT ON TABLE public.lug_tipo_lugar IS 'Tabla que contiene el tipo de lugar.';
-- ddl-end --
COMMENT ON COLUMN public.lug_tipo_lugar.id_tipo_lugar IS 'Identificador del tipo de lugar';
-- ddl-end --
COMMENT ON COLUMN public.lug_tipo_lugar.nombre IS 'Nombre del tipo de lugar';
-- ddl-end --
COMMENT ON COLUMN public.lug_tipo_lugar.descripcion IS 'Descripción del tipo de lugar.';
-- ddl-end --
ALTER TABLE public.lug_tipo_lugar OWNER TO postgres;
-- ddl-end --

-- object: public.usu_usuario | type: TABLE --
-- DROP TABLE IF EXISTS public.usu_usuario CASCADE;
CREATE TABLE public.usu_usuario(
	id_usuario serial NOT NULL,
	nombres varchar(100) NOT NULL,
	apellidos varchar(100) NOT NULL,
	correo varchar(100) NOT NULL,
	contrasenia varchar(100) NOT NULL,
	confirmacion_correo boolean,
	fecha_creacion timestamp,
	fecha_modificacion timestamp,
	sexo varchar(20),
	cuidad varchar(100),
	estado_civil varchar(20),
	fecha_nacimiento time,
	profesion varchar(100),
	pregunta_seguridad text,
	respuesta_seguridad text,
	id_nacionalidad integer,
	CONSTRAINT usuario_pk PRIMARY KEY (id_usuario)

);
-- ddl-end --
COMMENT ON TABLE public.usu_usuario IS 'Tabla que contiene los datos de los usuarios';
-- ddl-end --
COMMENT ON COLUMN public.usu_usuario.id_usuario IS 'Identificador del usuario';
-- ddl-end --
COMMENT ON COLUMN public.usu_usuario.nombres IS 'Primer y segundo nombre del usuario';
-- ddl-end --
COMMENT ON COLUMN public.usu_usuario.apellidos IS 'Primer y segundo apellido del usuario';
-- ddl-end --
COMMENT ON COLUMN public.usu_usuario.correo IS 'Correo electrónico del usuario';
-- ddl-end --
COMMENT ON COLUMN public.usu_usuario.contrasenia IS 'Contraseña del usuario';
-- ddl-end --
COMMENT ON COLUMN public.usu_usuario.confirmacion_correo IS 'Campo para registrar la confirmación del correo electronico.';
-- ddl-end --
COMMENT ON COLUMN public.usu_usuario.fecha_creacion IS 'Campo para registrar la fecha de creación del registro';
-- ddl-end --
COMMENT ON COLUMN public.usu_usuario.fecha_modificacion IS 'Campo para registrar la fecha de actualización de un registro';
-- ddl-end --
COMMENT ON COLUMN public.usu_usuario.sexo IS 'Campo para registrar el sexo del usuario';
-- ddl-end --
COMMENT ON COLUMN public.usu_usuario.cuidad IS 'Campo para registrar la cuidad de recidencia del usuario';
-- ddl-end --
COMMENT ON COLUMN public.usu_usuario.estado_civil IS 'Campo para registrar el estado civil del usuario';
-- ddl-end --
COMMENT ON COLUMN public.usu_usuario.fecha_nacimiento IS 'Campo para registrar la fecha de nacimiento del usuario.';
-- ddl-end --
COMMENT ON COLUMN public.usu_usuario.profesion IS 'Campo para registrar la profesion del usuario';
-- ddl-end --
COMMENT ON COLUMN public.usu_usuario.pregunta_seguridad IS 'Campo para registrar la pregunta de seguridad, para recuperar la contraseña del usuario.';
-- ddl-end --
COMMENT ON COLUMN public.usu_usuario.respuesta_seguridad IS 'Campo para registrar la respuesta de seguridad';
-- ddl-end --
ALTER TABLE public.usu_usuario OWNER TO postgres;
-- ddl-end --

-- object: public.usu_rol | type: TABLE --
-- DROP TABLE IF EXISTS public.usu_rol CASCADE;
CREATE TABLE public.usu_rol(
	id_rol serial NOT NULL,
	nombre varchar(100) NOT NULL,
	CONSTRAINT rol_pk PRIMARY KEY (id_rol)

);
-- ddl-end --
COMMENT ON TABLE public.usu_rol IS 'Esta tabla contiene los roles de los usuarios';
-- ddl-end --
COMMENT ON COLUMN public.usu_rol.id_rol IS 'Identificador del rol';
-- ddl-end --
COMMENT ON COLUMN public.usu_rol.nombre IS 'Nombre del rol';
-- ddl-end --
ALTER TABLE public.usu_rol OWNER TO postgres;
-- ddl-end --

-- object: public.lug_contacto | type: TABLE --
-- DROP TABLE IF EXISTS public.lug_contacto CASCADE;
CREATE TABLE public.lug_contacto(
	id_contacto serial NOT NULL,
	nombre varchar(50) NOT NULL,
	telefono varchar(15),
	celular varchar(20),
	correo varchar(200),
	id_lugar integer NOT NULL,
	CONSTRAINT contacto_pk PRIMARY KEY (id_contacto)

);
-- ddl-end --
COMMENT ON TABLE public.lug_contacto IS 'Tabla que contiene los datos de contacto de cada lugar';
-- ddl-end --
COMMENT ON COLUMN public.lug_contacto.id_contacto IS 'Identificador del contacto';
-- ddl-end --
COMMENT ON COLUMN public.lug_contacto.nombre IS 'Nombre del contacto';
-- ddl-end --
COMMENT ON COLUMN public.lug_contacto.telefono IS 'Telefono del contacto';
-- ddl-end --
COMMENT ON COLUMN public.lug_contacto.celular IS 'Campo para registrar el número celular';
-- ddl-end --
COMMENT ON COLUMN public.lug_contacto.correo IS 'Correo electrónico del contacto';
-- ddl-end --
ALTER TABLE public.lug_contacto OWNER TO postgres;
-- ddl-end --

-- object: public.inf_division_politica | type: TABLE --
-- DROP TABLE IF EXISTS public.inf_division_politica CASCADE;
CREATE TABLE public.inf_division_politica(
	id_division_politica serial NOT NULL,
	nombre varchar(100) NOT NULL,
	id_division_politica_fk integer,
	id_nivel integer NOT NULL,
	CONSTRAINT "divisionPolitica_pk" PRIMARY KEY (id_division_politica)

);
-- ddl-end --
COMMENT ON TABLE public.inf_division_politica IS 'Tabla que contiene la división politica del país.';
-- ddl-end --
COMMENT ON COLUMN public.inf_division_politica.id_division_politica IS 'Esta tabla contiene la división politica del país,';
-- ddl-end --
COMMENT ON COLUMN public.inf_division_politica.nombre IS 'Nombre del sector';
-- ddl-end --
COMMENT ON COLUMN public.inf_division_politica.id_division_politica_fk IS 'Clave foránea de division politica.';
-- ddl-end --
ALTER TABLE public.inf_division_politica OWNER TO postgres;
-- ddl-end --

-- object: public.mul_multimedia | type: TABLE --
-- DROP TABLE IF EXISTS public.mul_multimedia CASCADE;
CREATE TABLE public.mul_multimedia(
	id_multimedia serial NOT NULL,
	nombre varchar(200) NOT NULL,
	descripcion text,
	descripcion_corta varchar(250),
	direccion text NOT NULL,
	portada boolean,
	fecha_creacion timestamp,
	fecha_modificacion timestamp,
	id_lugar integer NOT NULL,
	id_tipo_archivo integer NOT NULL,
	CONSTRAINT multimedia_pk PRIMARY KEY (id_multimedia)

);
-- ddl-end --
COMMENT ON TABLE public.mul_multimedia IS 'Tabla que contiene los datos multimedia de cada lugar.';
-- ddl-end --
COMMENT ON COLUMN public.mul_multimedia.id_multimedia IS 'Identificador de cada artículo multimedia.';
-- ddl-end --
COMMENT ON COLUMN public.mul_multimedia.nombre IS 'Nombre de cada artículo multimedia.';
-- ddl-end --
COMMENT ON COLUMN public.mul_multimedia.descripcion IS 'Descripción de cada artículo multimedia.';
-- ddl-end --
COMMENT ON COLUMN public.mul_multimedia.descripcion_corta IS 'Campo para registrar una descripción máxima de 250 caracteres sobre la información multimedia.';
-- ddl-end --
COMMENT ON COLUMN public.mul_multimedia.direccion IS 'Dirección URL o de archivo de cada artículo multimedia.';
-- ddl-end --
COMMENT ON COLUMN public.mul_multimedia.portada IS 'Campo para registrar si la imagen se debe presentar como portada';
-- ddl-end --
COMMENT ON COLUMN public.mul_multimedia.fecha_creacion IS 'Campo para registrar la fecha de creacion de un registro';
-- ddl-end --
COMMENT ON COLUMN public.mul_multimedia.fecha_modificacion IS 'Campo para registrar la fecha de modificacion de un registro';
-- ddl-end --
ALTER TABLE public.mul_multimedia OWNER TO postgres;
-- ddl-end --

-- object: public.mul_calificacion | type: TABLE --
-- DROP TABLE IF EXISTS public.mul_calificacion CASCADE;
CREATE TABLE public.mul_calificacion(
	id_calificacion serial NOT NULL,
	valor int8 NOT NULL,
	observacion varchar(200),
	fecha_creacion timestamp,
	fecha_modificacion timestamp,
	id_lugar integer NOT NULL,
	id_usuario integer NOT NULL,
	CONSTRAINT calificacion_pk PRIMARY KEY (id_calificacion)

);
-- ddl-end --
COMMENT ON TABLE public.mul_calificacion IS 'Esta tabla contiene la calificación que da el usuario a cada lugar.';
-- ddl-end --
COMMENT ON COLUMN public.mul_calificacion.id_calificacion IS 'Identificador de la calificación.';
-- ddl-end --
COMMENT ON COLUMN public.mul_calificacion.valor IS 'Valor calificado.';
-- ddl-end --
COMMENT ON COLUMN public.mul_calificacion.observacion IS 'Comentario u observación del lugar.';
-- ddl-end --
COMMENT ON COLUMN public.mul_calificacion.fecha_creacion IS 'Campo para registrar la fecha de la calificación';
-- ddl-end --
COMMENT ON COLUMN public.mul_calificacion.fecha_modificacion IS 'Campo para registrar la fecha de modificación de un registro';
-- ddl-end --
ALTER TABLE public.mul_calificacion OWNER TO postgres;
-- ddl-end --

-- object: public.mul_tipo_archivo | type: TABLE --
-- DROP TABLE IF EXISTS public.mul_tipo_archivo CASCADE;
CREATE TABLE public.mul_tipo_archivo(
	id_tipo_archivo serial NOT NULL,
	nombre varchar(100),
	extension varchar(100),
	CONSTRAINT tipo_archivo_pk PRIMARY KEY (id_tipo_archivo)

);
-- ddl-end --
COMMENT ON TABLE public.mul_tipo_archivo IS 'Tabla para registrar los diferentes dipos de archivo.';
-- ddl-end --
COMMENT ON COLUMN public.mul_tipo_archivo.id_tipo_archivo IS 'Clave primaria la tabla tipo de archivo';
-- ddl-end --
COMMENT ON COLUMN public.mul_tipo_archivo.nombre IS 'Nombre del tipo de archivo';
-- ddl-end --
COMMENT ON COLUMN public.mul_tipo_archivo.extension IS 'Campo para registrar las extensiones de los diferentes tipos de archivos.';
-- ddl-end --
ALTER TABLE public.mul_tipo_archivo OWNER TO postgres;
-- ddl-end --

-- object: public.usu_usuario_rol | type: TABLE --
-- DROP TABLE IF EXISTS public.usu_usuario_rol CASCADE;
CREATE TABLE public.usu_usuario_rol(
	id_usuario_rol serial NOT NULL,
	id_usuario integer NOT NULL,
	id_rol integer NOT NULL,
	CONSTRAINT usuario_rol_pk PRIMARY KEY (id_usuario_rol)

);
-- ddl-end --
COMMENT ON TABLE public.usu_usuario_rol IS 'Tabla para registrar los diferentes roles que tienen los usuarios.';
-- ddl-end --
COMMENT ON COLUMN public.usu_usuario_rol.id_usuario_rol IS 'Clave primaria usuario y rol';
-- ddl-end --
ALTER TABLE public.usu_usuario_rol OWNER TO postgres;
-- ddl-end --

-- object: public.inf_parametro | type: TABLE --
-- DROP TABLE IF EXISTS public.inf_parametro CASCADE;
CREATE TABLE public.inf_parametro(
	id_parametro serial NOT NULL,
	nombre varchar(100) NOT NULL,
	valor text,
	CONSTRAINT parametro_pk PRIMARY KEY (id_parametro)

);
-- ddl-end --
COMMENT ON TABLE public.inf_parametro IS 'Tabla para registrar los parametros o información que se presentará en la página web.';
-- ddl-end --
COMMENT ON COLUMN public.inf_parametro.id_parametro IS 'Identificador unico del parametro';
-- ddl-end --
COMMENT ON COLUMN public.inf_parametro.nombre IS 'Campo para registrar el nombre del parametro';
-- ddl-end --
COMMENT ON COLUMN public.inf_parametro.valor IS 'Campo para registrar el valor del parametro';
-- ddl-end --
ALTER TABLE public.inf_parametro OWNER TO postgres;
-- ddl-end --

-- object: usu_usuario_fk | type: CONSTRAINT --
-- ALTER TABLE public.lug_lugar DROP CONSTRAINT IF EXISTS usu_usuario_fk CASCADE;
ALTER TABLE public.lug_lugar ADD CONSTRAINT usu_usuario_fk FOREIGN KEY (id_usuario)
REFERENCES public.usu_usuario (id_usuario) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: lug_tipo_lugar_fk | type: CONSTRAINT --
-- ALTER TABLE public.lug_lugar DROP CONSTRAINT IF EXISTS lug_tipo_lugar_fk CASCADE;
ALTER TABLE public.lug_lugar ADD CONSTRAINT lug_tipo_lugar_fk FOREIGN KEY (id_tipo_lugar)
REFERENCES public.lug_tipo_lugar (id_tipo_lugar) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: inf_division_politica_fk | type: CONSTRAINT --
-- ALTER TABLE public.lug_lugar DROP CONSTRAINT IF EXISTS inf_division_politica_fk CASCADE;
ALTER TABLE public.lug_lugar ADD CONSTRAINT inf_division_politica_fk FOREIGN KEY (id_division_politica)
REFERENCES public.inf_division_politica (id_division_politica) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: lug_lugar_fk | type: CONSTRAINT --
-- ALTER TABLE public.mul_multimedia DROP CONSTRAINT IF EXISTS lug_lugar_fk CASCADE;
ALTER TABLE public.mul_multimedia ADD CONSTRAINT lug_lugar_fk FOREIGN KEY (id_lugar)
REFERENCES public.lug_lugar (id_lugar) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: mul_tipo_archivo_fk | type: CONSTRAINT --
-- ALTER TABLE public.mul_multimedia DROP CONSTRAINT IF EXISTS mul_tipo_archivo_fk CASCADE;
ALTER TABLE public.mul_multimedia ADD CONSTRAINT mul_tipo_archivo_fk FOREIGN KEY (id_tipo_archivo)
REFERENCES public.mul_tipo_archivo (id_tipo_archivo) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: lug_lugar_fk | type: CONSTRAINT --
-- ALTER TABLE public.mul_calificacion DROP CONSTRAINT IF EXISTS lug_lugar_fk CASCADE;
ALTER TABLE public.mul_calificacion ADD CONSTRAINT lug_lugar_fk FOREIGN KEY (id_lugar)
REFERENCES public.lug_lugar (id_lugar) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: usu_usuario_fk | type: CONSTRAINT --
-- ALTER TABLE public.mul_calificacion DROP CONSTRAINT IF EXISTS usu_usuario_fk CASCADE;
ALTER TABLE public.mul_calificacion ADD CONSTRAINT usu_usuario_fk FOREIGN KEY (id_usuario)
REFERENCES public.usu_usuario (id_usuario) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: lug_lugar_fk | type: CONSTRAINT --
-- ALTER TABLE public.lug_contacto DROP CONSTRAINT IF EXISTS lug_lugar_fk CASCADE;
ALTER TABLE public.lug_contacto ADD CONSTRAINT lug_lugar_fk FOREIGN KEY (id_lugar)
REFERENCES public.lug_lugar (id_lugar) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: usu_rol_fk | type: CONSTRAINT --
-- ALTER TABLE public.usu_usuario_rol DROP CONSTRAINT IF EXISTS usu_rol_fk CASCADE;
ALTER TABLE public.usu_usuario_rol ADD CONSTRAINT usu_rol_fk FOREIGN KEY (id_rol)
REFERENCES public.usu_rol (id_rol) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: usu_usuario_fk | type: CONSTRAINT --
-- ALTER TABLE public.usu_usuario_rol DROP CONSTRAINT IF EXISTS usu_usuario_fk CASCADE;
ALTER TABLE public.usu_usuario_rol ADD CONSTRAINT usu_usuario_fk FOREIGN KEY (id_usuario)
REFERENCES public.usu_usuario (id_usuario) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: id_usuario_rol_uq | type: CONSTRAINT --
-- ALTER TABLE public.usu_usuario_rol DROP CONSTRAINT IF EXISTS id_usuario_rol_uq CASCADE;
ALTER TABLE public.usu_usuario_rol ADD CONSTRAINT id_usuario_rol_uq UNIQUE (id_usuario,id_rol);
-- ddl-end --

-- object: public.usu_bitacora | type: TABLE --
-- DROP TABLE IF EXISTS public.usu_bitacora CASCADE;
CREATE TABLE public.usu_bitacora(
	codigo_evento serial NOT NULL,
	fecha_evento timestamp NOT NULL,
	metodo varchar(100) NOT NULL,
	descripcion varchar(200),
	direccion_ip varchar(20),
	id_usuario integer NOT NULL,
	CONSTRAINT usu_bitacora_pk PRIMARY KEY (codigo_evento)

);
-- ddl-end --
COMMENT ON TABLE public.usu_bitacora IS 'Tabla para registrar los eventos que ejecuta un usuario en el sistema.';
-- ddl-end --
COMMENT ON COLUMN public.usu_bitacora.codigo_evento IS 'Campo para registrar el código de un evento. Clave primaria.';
-- ddl-end --
COMMENT ON COLUMN public.usu_bitacora.fecha_evento IS 'Campo para registrar la fecha y hora en la que se ejecutó el evento.';
-- ddl-end --
COMMENT ON COLUMN public.usu_bitacora.metodo IS 'Registra el método que ejecutó el evento.';
-- ddl-end --
COMMENT ON COLUMN public.usu_bitacora.descripcion IS 'Registra la información detallada del evento.';
-- ddl-end --
COMMENT ON COLUMN public.usu_bitacora.direccion_ip IS 'Registra la dirección IP donde de generó el evento.';
-- ddl-end --
ALTER TABLE public.usu_bitacora OWNER TO postgres;
-- ddl-end --

-- object: usu_usuario_fk | type: CONSTRAINT --
-- ALTER TABLE public.usu_bitacora DROP CONSTRAINT IF EXISTS usu_usuario_fk CASCADE;
ALTER TABLE public.usu_bitacora ADD CONSTRAINT usu_usuario_fk FOREIGN KEY (id_usuario)
REFERENCES public.usu_usuario (id_usuario) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: public.lug_reporte | type: TABLE --
-- DROP TABLE IF EXISTS public.lug_reporte CASCADE;
CREATE TABLE public.lug_reporte(
	id_reporte serial NOT NULL,
	observacion text,
	fecha_creacion timestamp,
	fecha_modificacion timestamp,
	id_estado_reporte integer NOT NULL,
	id_usuario integer,
	id_lugar integer NOT NULL,
	CONSTRAINT lug_reporte_pk PRIMARY KEY (id_reporte)

);
-- ddl-end --
COMMENT ON TABLE public.lug_reporte IS 'Tabla para registrar el reporte de algún sucedo en los datos de los lugares.';
-- ddl-end --
COMMENT ON COLUMN public.lug_reporte.id_reporte IS 'Clave primaria';
-- ddl-end --
COMMENT ON COLUMN public.lug_reporte.observacion IS 'Campo para registrar el comentarion';
-- ddl-end --
COMMENT ON COLUMN public.lug_reporte.fecha_creacion IS 'Registrar la fecha en que se creó el comentario';
-- ddl-end --
COMMENT ON COLUMN public.lug_reporte.fecha_modificacion IS 'Campo para registrar la fecha de modificación de un registro';
-- ddl-end --
ALTER TABLE public.lug_reporte OWNER TO postgres;
-- ddl-end --

-- object: public.lug_estado_reporte | type: TABLE --
-- DROP TABLE IF EXISTS public.lug_estado_reporte CASCADE;
CREATE TABLE public.lug_estado_reporte(
	id_estado_reporte serial NOT NULL,
	nombre varchar(20),
	CONSTRAINT lug_estado_reporte_pk PRIMARY KEY (id_estado_reporte)

);
-- ddl-end --
COMMENT ON TABLE public.lug_estado_reporte IS 'Tabla para registrar los estados de un reporte.';
-- ddl-end --
COMMENT ON COLUMN public.lug_estado_reporte.id_estado_reporte IS 'Clave primaria';
-- ddl-end --
COMMENT ON COLUMN public.lug_estado_reporte.nombre IS 'Nombre del estado';
-- ddl-end --
ALTER TABLE public.lug_estado_reporte OWNER TO postgres;
-- ddl-end --

-- object: lug_estado_reporte_fk | type: CONSTRAINT --
-- ALTER TABLE public.lug_reporte DROP CONSTRAINT IF EXISTS lug_estado_reporte_fk CASCADE;
ALTER TABLE public.lug_reporte ADD CONSTRAINT lug_estado_reporte_fk FOREIGN KEY (id_estado_reporte)
REFERENCES public.lug_estado_reporte (id_estado_reporte) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: usu_usuario_fk | type: CONSTRAINT --
-- ALTER TABLE public.lug_reporte DROP CONSTRAINT IF EXISTS usu_usuario_fk CASCADE;
ALTER TABLE public.lug_reporte ADD CONSTRAINT usu_usuario_fk FOREIGN KEY (id_usuario)
REFERENCES public.usu_usuario (id_usuario) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --

-- object: lug_lugar_fk | type: CONSTRAINT --
-- ALTER TABLE public.lug_reporte DROP CONSTRAINT IF EXISTS lug_lugar_fk CASCADE;
ALTER TABLE public.lug_reporte ADD CONSTRAINT lug_lugar_fk FOREIGN KEY (id_lugar)
REFERENCES public.lug_lugar (id_lugar) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: public.inf_nivel | type: TABLE --
-- DROP TABLE IF EXISTS public.inf_nivel CASCADE;
CREATE TABLE public.inf_nivel(
	id_nivel serial NOT NULL,
	nombre varchar(100),
	CONSTRAINT inf_nivel_pk PRIMARY KEY (id_nivel)

);
-- ddl-end --
COMMENT ON TABLE public.inf_nivel IS 'Tabla para registrar los niveles de las divisiones política';
-- ddl-end --
COMMENT ON COLUMN public.inf_nivel.id_nivel IS 'Clave primaria de la tabla nivel';
-- ddl-end --
COMMENT ON COLUMN public.inf_nivel.nombre IS 'Campo para registrar el nombre del nivel.';
-- ddl-end --
ALTER TABLE public.inf_nivel OWNER TO postgres;
-- ddl-end --

-- object: inf_nivel_fk | type: CONSTRAINT --
-- ALTER TABLE public.inf_division_politica DROP CONSTRAINT IF EXISTS inf_nivel_fk CASCADE;
ALTER TABLE public.inf_division_politica ADD CONSTRAINT inf_nivel_fk FOREIGN KEY (id_nivel)
REFERENCES public.inf_nivel (id_nivel) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: public.usu_nacionalidad | type: TABLE --
-- DROP TABLE IF EXISTS public.usu_nacionalidad CASCADE;
CREATE TABLE public.usu_nacionalidad(
	id_nacionalidad serial NOT NULL,
	nombre varchar(100),
	CONSTRAINT nacionalidad_pk PRIMARY KEY (id_nacionalidad)

);
-- ddl-end --
COMMENT ON TABLE public.usu_nacionalidad IS 'Tabla para registrar las nacionalidades de los usuarios';
-- ddl-end --
COMMENT ON COLUMN public.usu_nacionalidad.id_nacionalidad IS 'Clave primaria';
-- ddl-end --
COMMENT ON COLUMN public.usu_nacionalidad.nombre IS 'Campo para registrar el nombre de la nacionalidad';
-- ddl-end --
ALTER TABLE public.usu_nacionalidad OWNER TO postgres;
-- ddl-end --

-- object: usu_nacionalidad_fk | type: CONSTRAINT --
-- ALTER TABLE public.usu_usuario DROP CONSTRAINT IF EXISTS usu_nacionalidad_fk CASCADE;
ALTER TABLE public.usu_usuario ADD CONSTRAINT usu_nacionalidad_fk FOREIGN KEY (id_nacionalidad)
REFERENCES public.usu_nacionalidad (id_nacionalidad) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --

-- object: "divisionPolitica_fk" | type: CONSTRAINT --
-- ALTER TABLE public.inf_division_politica DROP CONSTRAINT IF EXISTS "divisionPolitica_fk" CASCADE;
ALTER TABLE public.inf_division_politica ADD CONSTRAINT "divisionPolitica_fk" FOREIGN KEY (id_division_politica_fk)
REFERENCES public.inf_division_politica (id_division_politica) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --


