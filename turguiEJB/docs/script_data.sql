


----- INSERT ROL -----
INSERT INTO public.usu_rol(nombre) VALUES ('superadministrador');
INSERT INTO public.usu_rol(nombre) VALUES ('administrador');
INSERT INTO public.usu_rol(nombre) VALUES ('usuario');

------ INSERT USUARIO ------
INSERT INTO public.usu_usuario(nombres, apellidos, correo, contrasenia, fecha_creacion, fecha_modificacion)
    VALUES ('Neider', 'Requene', 'requeneneider@gmail.com', '12345678', '03/01/2020', '03/01/2020');
INSERT INTO public.usu_usuario(nombres, apellidos, correo, contrasenia, fecha_creacion, fecha_modificacion)
    VALUES ('Alejandro', 'Arias', 'ariasalejandro@gmail.com', '12345678', '03/01/2020', '03/01/2020');
INSERT INTO public.usu_usuario(nombres, apellidos, correo, contrasenia, fecha_creacion, fecha_modificacion)
    VALUES ('Andres', 'Jacome', 'jacomeandres@gmail.com', '12345678', '03/01/2020', '03/01/2020');
INSERT INTO public.usu_usuario(nombres, apellidos, correo, contrasenia, fecha_creacion, fecha_modificacion)
    VALUES ('Jayli', 'De la torre', 'delatorrejayli@gmail.com', '12345678', '03/01/2020', '03/01/2020');
    
------ INSERT USUARIO ROL ------
INSERT INTO public.usu_usuario_rol(id_usuario, id_rol) VALUES (1, 1);
INSERT INTO public.usu_usuario_rol(id_usuario, id_rol) VALUES (2, 2);
INSERT INTO public.usu_usuario_rol(id_usuario, id_rol) VALUES (3, 2);
INSERT INTO public.usu_usuario_rol(id_usuario, id_rol) VALUES (4, 2);


------ INSERT TIPO LUGAR -----
INSERT INTO public.lug_tipo_lugar(nombre, descripcion) VALUES ('Lagunas','Lagunas de origen volcánico');
INSERT INTO public.lug_tipo_lugar(nombre, descripcion) VALUES ('Hoteles','Hoteles turisticos');
INSERT INTO public.lug_tipo_lugar(nombre, descripcion) VALUES ('Cascadas','Cascadas de la sierra');


----- INSERT TIPO ARCHIVO ----
INSERT INTO public.mul_tipo_archivo(nombre) VALUES ('IMAGEN');
INSERT INTO public.mul_tipo_archivo(nombre) VALUES ('VIDEO');


----- INSERT PARAMETRO ------
INSERT INTO public.inf_parametro(nombre, valor)VALUES ('titulo_pagina', 'Guía turística');
INSERT INTO public.inf_parametro(nombre, valor)VALUES ('facebook', 'https://facebook.com');
INSERT INTO public.inf_parametro(nombre, valor)VALUES ('gmail', 'https://gmail.com');


----- INSERT NIVEL -----
INSERT INTO public.inf_nivel(nombre) VALUES ('PROVINCIA');
INSERT INTO public.inf_nivel(nombre) VALUES ('CANTÓN');
INSERT INTO public.inf_nivel(nombre) VALUES ('PARROQUIA');
----- INSERT DIVISION POLITICA -----
INSERT INTO public.inf_division_politica(nombre, id_division_politica_fk, id_nivel)VALUES ('Imbabura', null, 1);
INSERT INTO public.inf_division_politica(nombre, id_division_politica_fk, id_nivel)VALUES ('Ibarra', 1,2);

------ INSERT LUGAR -----
INSERT INTO public.lug_lugar(nombre, direccion, referencia, descripcion, latitud, longitud, fecha_creacion, fecha_modificacion, 
	id_usuario, id_tipo_lugar, id_division_politica)
    VALUES ('Laguna de Yahuarcocha', 'Ibarra', 'Salida Norte de la ciudad de Ibarra', 'Laguna turística de Ibarra', 
    '-0.225219', '-78.5248', '03/01/2020', '03/01/2020', 2, 1, 1);
    
------ INSERT MULTIMEDIA -----
INSERT INTO public.mul_multimedia(nombre, descripcion, direccion, fecha_creacion, fecha_modificacion, id_lugar, id_tipo_archivo)
    VALUES ('Laguna Yahuarcocha', 'Laguna turística de la cuidad de Ibarra', '/multimedia/fotos/img1.jpg','03/01/2020', '03/01/2020', 1, 1);
    
----- INSERT CONTACTO ----
INSERT INTO public.lug_contacto(nombre, telefono, correo, id_lugar)
    VALUES ('Neider', '0984986158', 'nfrequenee@utn.edu.ec', 1);


------ INSERT CALIFICACION ----
INSERT INTO public.mul_calificacion(valor, observacion, fecha_creacion, fecha_modificacion, id_lugar, id_usuario)
    VALUES (2,'Lindo lugar', '03/01/2020', '03/01/2020', 1, 1);
