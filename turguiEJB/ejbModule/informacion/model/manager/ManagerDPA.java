package informacion.model.manager;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import turgui.model.entities.InfDivisionPolitica;
import turgui.model.entities.InfNivel;


/**
 * Session Bean implementation class ManagerDPA
 */
@Stateless
@LocalBean
public class ManagerDPA {

	/**
	 * Default constructor.
	 */
	@PersistenceContext

	private EntityManager em;

	public ManagerDPA() {
		// TODO Auto-generated constructor stub
	}

	public List<InfDivisionPolitica> findAllInfDPA() {
		return em.createNamedQuery("InfDivisionPolitica.findAll").getResultList();
	}

	public List<InfNivel> findAllLevels() {
		return em.createNamedQuery("InfNivel.findAll").getResultList();
	}

	public List<InfDivisionPolitica> findAllByNIvel(int idNivel) {
		String consulta = "SELECT m from InfDivisionPolitica m  WHERE m.infNivel.idNivel=" + idNivel;
		Query q = em.createQuery(consulta);
		return q.getResultList();
	}

	public List<InfDivisionPolitica> findInfDivisonPoliticaByIdNivelAndIdDivisionPolitica(int idNivel,
			int idDivisionPolitica) {
		String consulta = "SELECT m from InfDivisionPolitica m  WHERE m.infNivel.idNivel=" + idNivel
				+ " and m.infDivisionPolitica.idDivisionPolitica=" + idDivisionPolitica;
		Query q = em.createQuery(consulta);
		return q.getResultList();
	}

	public void insertDPA(String nombre, int nivelSeleccionado, int idDpaSeleccionado) {
		InfDivisionPolitica dpa = new InfDivisionPolitica();
		dpa.setNombre(nombre);

		InfNivel nv = em.find(InfNivel.class, nivelSeleccionado);
		dpa.setInfNivel(nv);
		
		InfDivisionPolitica dpaConsulta = em.find(InfDivisionPolitica.class, idDpaSeleccionado);
		dpa.setInfDivisionPolitica(dpaConsulta);
		
		em.persist(dpa);

	}

	public boolean deleteDpa(int idDPA) {
		InfDivisionPolitica infDivisionPolitica = findInfDivisionById(idDPA);
		if(findInfDivisonPoliticaFK(infDivisionPolitica.getIdDivisionPolitica()).size()>0) {
			return false;
		}
		if (infDivisionPolitica != null) {
			em.remove(infDivisionPolitica);
		}
		return true;
	}
	public List<InfDivisionPolitica> findInfDivisonPoliticaFK(int idDivisionPolitica) {
		String consulta = "SELECT m from InfDivisionPolitica m  WHERE m.infDivisionPolitica.idDivisionPolitica=" + idDivisionPolitica;
		Query q = em.createQuery(consulta);
		return q.getResultList();
	}
	
	public void updateInfDPA(InfDivisionPolitica infDPA) throws Exception {
		InfDivisionPolitica nuevo_dpa = findInfDivisionById(infDPA.getIdDivisionPolitica());
		if (nuevo_dpa == null) {
			throw new Exception("NO existe el parametro");
		} else {
			nuevo_dpa.setNombre(infDPA.getNombre());
			em.merge(nuevo_dpa);
		}
	}

	public InfDivisionPolitica findInfDivisionById(int idDPA) {
		return em.find(InfDivisionPolitica.class, idDPA);
	}

}
