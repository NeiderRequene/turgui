package informacion.model.manager;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import turgui.model.entities.InfParametro;


/**
 * Session Bean implementation class ManagerInfParametro
 */
@Stateless
@LocalBean
public class ManagerInformacion {

	/**
	 * Default constructor.
	 */
	@PersistenceContext
	private EntityManager em;

	public ManagerInformacion() {
		// TODO Auto-generated constructor stub
	}

	public List<InfParametro> findAllInfParametros() {
		return em.createNamedQuery("InfParametro.findAll").getResultList();
	}

	public InfParametro findInfParametroById(int idInfParametro) {
		return em.find(InfParametro.class, idInfParametro);
	}

	public List<InfParametro> findInfParametroByNombre(String nombreInfParametro) {
		String capsula = " m.nombre = '" + nombreInfParametro + "'";
		String consulta = "SELECT m from InfParametro  m WHERE " + capsula;
		Query q = em.createQuery(consulta);
		return q.getResultList();
	}

	public String insertParametro(InfParametro infParametro) throws Exception {
		
		if (findInfParametroByNombre(infParametro.getNombre()).size() > 0) {
			throw new Exception("Ya existe el nombre del Parametro");
		} else {
			em.persist(infParametro);
		}
		
		return "Se ingreso Parametro correcto";
	}

	public String deleteParametro(int idInfParametro) {
		InfParametro infParametro = findInfParametroById(idInfParametro);
		if (infParametro != null) {
			em.remove(infParametro);
		}
		
		return "Registro Eliminado";
	}

	public String updateInfParametro(InfParametro infParametro) throws Exception {
		InfParametro nuevo_infParametro = findInfParametroById(infParametro.getIdParametro());
		if (nuevo_infParametro == null) {
			throw new Exception("NO existe el parametro");
		} else {
			nuevo_infParametro.setNombre(infParametro.getNombre());
			nuevo_infParametro.setValor(infParametro.getValor());

			em.merge(nuevo_infParametro);
		}
		
		return "Registro actualizado";
	}
	


}
