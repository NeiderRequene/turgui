package informacion.model.manager;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import turgui.model.entities.InfNivel;
import turgui.model.entities.InfParametro;

/**
 * Session Bean implementation class ManagerNivel
 */
@Stateless
@LocalBean
public class ManagerNivel {

	@PersistenceContext
	private EntityManager em;
	
    /**
     * Default constructor. 
     */
    public ManagerNivel() {
        // TODO Auto-generated constructor stub
    }
    
    public List<InfNivel> findAllInfnivels() {
		return em.createNamedQuery("InfNivel.findAll").getResultList();
	}

	

	public List<InfNivel> findInfNivelByNombre(String nombreNivel) {
		String capsula = " m.nombre = '" + nombreNivel + "'";
		String consulta = "SELECT m from InfNivel  m WHERE " + capsula;
		Query q = em.createQuery(consulta);
		return q.getResultList();
	}

	public void insertNivel(InfNivel infNivel) throws Exception {

		if (findInfNivelByNombre(infNivel.getNombre()).size() > 0) {
			throw new Exception("Ya existe el nombre del Nivel");
		} else {
			em.persist(infNivel);
		}
	}

	public InfNivel findInfNivelById(int idInfNivel) {
		return em.find(InfNivel.class, idInfNivel);
	}

	
	public void deleteNivel(int idInfNivel) {
		InfNivel infNivel = findInfNivelById(idInfNivel);
		if (infNivel != null) {
			em.remove(infNivel);
		}
	}

	public void updateInfNivel(InfNivel infNivel) throws Exception {
		InfNivel nuevo_infNivel = findInfNivelById(infNivel.getIdNivel());
		if (nuevo_infNivel == null) {
			throw new Exception("NO existe el parametro");
		} else {
			nuevo_infNivel.setNombre(infNivel.getNombre());
			

			em.merge(nuevo_infNivel);
		}
	}
    
    

}
