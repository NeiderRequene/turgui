package lugar.model.manager;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import turgui.model.entities.LugContacto;
import turgui.model.entities.LugLugar;

/**
 * Session Bean implementation class ManagerContacto
 */
@Stateless
@LocalBean
public class ManagerContacto {
	@PersistenceContext
	private EntityManager em;
	
    /**
     * Default constructor. 
     */
    public ManagerContacto() {
        // TODO Auto-generated constructor stub
    }
    
    public List<LugContacto> consutarContactos() {
    	TypedQuery<LugContacto> q = em.createNamedQuery("LugContacto.findAll", LugContacto.class);
    	return q.getResultList();
    }
    
    public void ingresarContacto(LugLugar lugar, String nombre, String telefono, String celular, String correo) throws Exception {
    	LugContacto contacto = new LugContacto();
    	contacto.setLugLugar(lugar);
    	contacto.setNombre(nombre);
    	contacto.setTelefono(telefono);
    	contacto.setCelular(celular);
    	contacto.setCorreo(correo);
    	
    	if (existeNombre(contacto)) {
    		throw new Exception("Ya existe el contacto");
    	}
    	em.persist(contacto);
    }
    
    public void eliminarContacto(int idContacto) {
    	LugContacto contacto = buscarContactoPorId(idContacto);
    	if (contacto != null) {
    		em.remove(contacto);
    	}
    }
    
    public void actualizarContacto(int idContacto, String nombre, String telefono, String celular, String correo, LugLugar lugar) throws Exception {
    	LugContacto contacto = buscarContactoPorId(idContacto);
    	if (contacto == null) {
    		throw new Exception("No existe el contacto");
    	}
    	contacto.setNombre(nombre);
    	contacto.setTelefono(telefono);
    	contacto.setCelular(celular);
    	contacto.setCorreo(correo);
    	contacto.setLugLugar(lugar);
    	
    	em.merge(contacto);
    }
    
    public LugContacto buscarContactoPorId(int idContacto) {
    	return em.find(LugContacto.class, idContacto);
    }
     
    public boolean existeNombre(LugContacto contacto) {
    	List<LugContacto> listaContactos = buscarContactoPorNombre(contacto.getNombre());
    	return listaContactos.size() > 0;
    }
    
    public List<LugContacto> buscarContactoPorNombre(String nombre) {
    	String consulta = "SELECT l FROM LugContacto l where l.nombre = :nombre";
    	TypedQuery<LugContacto> q = em.createQuery(consulta, LugContacto.class);
    	return q.setParameter("nombre", nombre).getResultList();
    }

}
