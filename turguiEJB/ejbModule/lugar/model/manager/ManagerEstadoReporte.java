package lugar.model.manager;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import turgui.model.entities.LugEstadoReporte;

/**
 * Session Bean implementation class ManagerEstadoReporte
 */
@Stateless
@LocalBean
public class ManagerEstadoReporte {
	@PersistenceContext
	private EntityManager em;
	
    /**
     * Default constructor. 
     */
    public ManagerEstadoReporte() {
        // TODO Auto-generated constructor stub
    }
    
    public List<LugEstadoReporte> consultarEstadoReporte() {
    	TypedQuery<LugEstadoReporte> q = em.createNamedQuery("LugEstadoReporte.findAll", LugEstadoReporte.class);
    	return q.getResultList();
    }
    
    public void ingresarEstadoReporte(String nombre) throws Exception {
    	LugEstadoReporte estadoReporte = new LugEstadoReporte();
    	estadoReporte.setNombre(nombre);
    	if (existeNombre(estadoReporte)) {
    		throw new Exception("Ya existe el Estado Reporte.");
    	}
    	em.persist(estadoReporte);
    }
    
    public void eliminarEstadoReporte(int idEstadoReporte) {
    	LugEstadoReporte estadoReporte = buscarEstadoReportePorId(idEstadoReporte);
    	if (estadoReporte != null) {
    		em.remove(estadoReporte);
    	}
    }
    
    public void actualizarEstadoReporte(LugEstadoReporte estadoReporte) throws Exception {
    	LugEstadoReporte viejoEstadoReporte = buscarEstadoReportePorId(estadoReporte.getIdEstadoReporte());
    	if (viejoEstadoReporte == null) {
    		throw new Exception("No existe el Estado Reporte.");
    	}
    	viejoEstadoReporte.setNombre(estadoReporte.getNombre());
    	em.merge(viejoEstadoReporte);
    }
    
    public LugEstadoReporte buscarEstadoReportePorId(int idEstadoReporte) {
    	return em.find(LugEstadoReporte.class, idEstadoReporte);
    }
    
    public boolean existeNombre(LugEstadoReporte estadoReporte) {
    	List<LugEstadoReporte> listaEstadoReporte = buscarEstadoReportePorNombre(estadoReporte.getNombre());
    	return listaEstadoReporte.size() > 0;
    }
    
    public List<LugEstadoReporte> buscarEstadoReportePorNombre(String nombre) {
    	String consulta = "SELECT l FROM LugEstadoReporte l WHERE l.nombre = :nombre";
    	TypedQuery<LugEstadoReporte> q = em.createQuery(consulta, LugEstadoReporte.class);
    	return q.setParameter("nombre", nombre).getResultList();
    }
}
