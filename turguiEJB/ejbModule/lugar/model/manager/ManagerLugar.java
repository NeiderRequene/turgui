package lugar.model.manager;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import turgui.model.dto.CalificacionDTO;
import turgui.model.dto.ContactoDTO;
import turgui.model.dto.LugarCardDTO;
import turgui.model.dto.LugarDTO;
import turgui.model.dto.MultimediaDTO;
import turgui.model.entities.InfDivisionPolitica;
import turgui.model.entities.LugContacto;
import turgui.model.entities.LugLugar;
import turgui.model.entities.LugTipoLugar;
import turgui.model.entities.MulCalificacion;
import turgui.model.entities.MulMultimedia;
import turgui.model.entities.UsuUsuario;

/**
 * Session Bean implementation class ManagerLugar
 */
@Stateless
@LocalBean
public class ManagerLugar {
	@PersistenceContext
	private EntityManager em;

	/**
	 * Default constructor.
	 */
	public ManagerLugar() {
		// TODO Auto-generated constructor stub
	}

	public List<LugLugar> consultarLugar() {
		TypedQuery<LugLugar> q = em.createNamedQuery("LugLugar.findAll", LugLugar.class);
		return q.getResultList();
	}
	public int findNumeroLugares() {
		TypedQuery<LugLugar> q = em.createNamedQuery("LugLugar.findAll", LugLugar.class);
		return q.getResultList().size();
	}
	
	public List<LugarCardDTO> consultarLugaresCard() {
		TypedQuery<LugLugar> q = em.createNamedQuery("LugLugar.findAll", LugLugar.class);
		List<LugLugar> lugares = q.getResultList();
		List<LugarCardDTO> cardDTOs = new ArrayList<LugarCardDTO>();
		for (LugLugar lugar : lugares) {
			LugarCardDTO cardDTO = new LugarCardDTO();
			cardDTO.setCalificacionPromedio(calcularCalificacionPromedio(lugar.getMulCalificacions()));
			cardDTO.setDescripcion(lugar.getDescripcion());
			cardDTO.setDescripcionCorta(lugar.getDescripcionCorta());
			cardDTO.setFechaCreacion(lugar.getFechaCreacion());
			cardDTO.setFechaModificacion(lugar.getFechaModificacion());
			cardDTO.setNombre(lugar.getNombre());
			cardDTO.setPrioridad(lugar.getPrioridad());
			cardDTO.setIdLugar(lugar.getIdLugar());
			cardDTO.setImagenPortada(buscarImagenPortada(lugar.getMulMultimedias()));
			cardDTOs.add(cardDTO);
		}
		return cardDTOs;
	}
	
	private int calcularCalificacionPromedio(List<MulCalificacion> calificaciones) {
		int cont = 0;
		int sum = 0;
		for (MulCalificacion calificacion : calificaciones) {
			sum = sum + Integer.parseInt(calificacion.getValor().toString());
			cont++;
		}
		if (cont == 0) return 0;
		return Math.floorDiv(sum, cont);
	}
	
	private String buscarImagenPortada(List<MulMultimedia> multimedias) {
		String portada = "../resources/images/not_found.svg";
		for (MulMultimedia multimedia : multimedias) {
			portada = multimedia.getDireccion();
			if (multimedia.getPortada() != null && multimedia.getPortada()) break;
		}
		return portada;
	}

	public void ingresarLugar(String nombre, String direccion, String referencia, String descripcion, String descripcionCorta, String latitud, String longitud, String codigoPostal, int prioridad, UsuUsuario usuario, LugTipoLugar tipoLugar, InfDivisionPolitica divisionPolitica) throws Exception {
		LugLugar lugar = new LugLugar();
		lugar.setNombre(nombre);
		lugar.setDireccion(direccion);
		lugar.setReferencia(referencia);
		lugar.setDescripcion(descripcion);
		lugar.setDescripcionCorta(descripcionCorta);
		lugar.setLatitud(latitud);
		lugar.setLongitud(longitud);
		lugar.setCodigoPostal(codigoPostal);
		lugar.setPrioridad(prioridad);
		lugar.setFechaCreacion(new Timestamp((new Date()).getTime()));
		lugar.setUsuUsuario(usuario);
		lugar.setLugTipoLugar(tipoLugar);
		lugar.setInfDivisionPolitica(divisionPolitica);
		
		if (existeNombre(lugar)) {
			throw new Exception("Ya existe el Lugar");
		}
		em.persist(lugar);
	}

	public void eliminarLugar(int idLugar) throws Exception {
		LugLugar lugar = buscarLugarPorId(idLugar);
		if (lugar != null) {
			em.remove(lugar);
		}
	}

	public void actualizarLugar(LugLugar lugar) throws Exception {
		LugLugar viejoLugar = buscarLugarPorId(lugar.getIdLugar());
		if (viejoLugar == null) {
			throw new Exception("No existe el Lugar.");
		}
		viejoLugar.setNombre(lugar.getNombre());
		viejoLugar.setDireccion(lugar.getDireccion());
		viejoLugar.setReferencia(lugar.getReferencia());
		viejoLugar.setDescripcion(lugar.getDescripcion());
		viejoLugar.setDescripcionCorta(lugar.getDescripcionCorta());
		viejoLugar.setLatitud(lugar.getLatitud());
		viejoLugar.setLongitud(lugar.getLongitud());
		viejoLugar.setCodigoPostal(lugar.getCodigoPostal());
		viejoLugar.setPrioridad(lugar.getPrioridad());
		viejoLugar.setFechaCreacion(lugar.getFechaCreacion());
		viejoLugar.setFechaModificacion(new Timestamp((new Date()).getTime())); // timestamp
		viejoLugar.setUsuUsuario(lugar.getUsuUsuario());
		viejoLugar.setLugTipoLugar(lugar.getLugTipoLugar());
		viejoLugar.setInfDivisionPolitica(lugar.getInfDivisionPolitica());
		
		em.merge(viejoLugar);
	}

	public LugLugar buscarLugarPorId(int idLugar) {
		return em.find(LugLugar.class, idLugar);
	}
	
	public LugarDTO buscarLugarDTOPorId(int idLugar) {
		LugLugar lugar = em.find(LugLugar.class, idLugar);
		LugarDTO lugarDTO = new LugarDTO();
		lugarDTO.setIdLugar(lugar.getIdLugar());
		lugarDTO.setCodigoPostal(lugar.getCodigoPostal());
		lugarDTO.setDescripcion(lugar.getDescripcion());
		lugarDTO.setDescripcionCorta(lugar.getDescripcionCorta());
		lugarDTO.setDireccion(lugar.getDireccion());
		lugarDTO.setFechaCreacion(lugar.getFechaCreacion());
		lugarDTO.setFechaModificacion(lugar.getFechaModificacion()); // timestamp
		lugarDTO.setLatitud(lugar.getLatitud());
		lugarDTO.setLongitud(lugar.getLongitud());
		lugarDTO.setNombre(lugar.getNombre());
		lugarDTO.setPrioridad(lugar.getPrioridad());
		lugarDTO.setReferencia(lugar.getReferencia());
		List<ContactoDTO> contactoDTOs = new ArrayList<>();
		for (LugContacto contacto : lugar.getLugContactos()) {
			ContactoDTO contactoDTO = new ContactoDTO();
			contactoDTO.setCelular(contacto.getCelular());
			contactoDTO.setCorreo(contacto.getCorreo());
			contactoDTO.setIdContacto(contacto.getIdContacto());
			contactoDTO.setNombre(contacto.getNombre());
			contactoDTO.setTelefono(contacto.getTelefono());
			contactoDTOs.add(contactoDTO);
		}
		lugarDTO.setLugContactos(contactoDTOs);
		lugarDTO.setInfDivisionPolitica(lugar.getInfDivisionPolitica());
		lugarDTO.setLugTipoLugar(lugar.getLugTipoLugar());
		List<MultimediaDTO> multimediaDTOs = new ArrayList<>();
		for (MulMultimedia multimedia : lugar.getMulMultimedias()) {
			MultimediaDTO multimediaDTO = new MultimediaDTO();
			multimediaDTO.setDescripcion(multimedia.getDescripcion());
			multimediaDTO.setDescripcionCorta(multimedia.getDescripcionCorta());
			multimediaDTO.setDireccion(multimedia.getDireccion());
			multimediaDTO.setFechaCreacion(multimedia.getFechaCreacion());
			multimediaDTO.setFechaModificacion(multimedia.getFechaModificacion());
			multimediaDTO.setIdMultimedia(multimedia.getIdMultimedia());
			multimediaDTO.setNombre(multimedia.getNombre());
			multimediaDTO.setPortada(multimedia.getPortada());
			multimediaDTOs.add(multimediaDTO);
		}
		lugarDTO.setMulMultimedias(multimediaDTOs);
		List<CalificacionDTO> calificacionDTOs = new ArrayList<>();
		for (MulCalificacion calificacion : lugar.getMulCalificacions()) {
			CalificacionDTO calificacionDTO = new CalificacionDTO();
			calificacionDTO.setFechaCreacion(calificacion.getFechaCreacion());
			calificacionDTO.setFechaModificacion(calificacion.getFechaModificacion());
			calificacionDTO.setIdCalificacion(calificacion.getIdCalificacion());
			calificacionDTO.setObservacion(calificacion.getObservacion());
			calificacionDTO.setValor(calificacion.getValor());
			calificacionDTOs.add(calificacionDTO);
		}
		lugarDTO.setMulCalificacions(calificacionDTOs);
		return lugarDTO;
	}

	public boolean existeNombre(LugLugar lugar) {
		List<LugLugar> listaLugar = buscarLugarPorNombre(lugar.getNombre());
		return listaLugar.size() > 0;
	}
	
	public List<LugContacto> buscarContactoById(int idLugar) {
		String consulta = "SELECT c FROM LugContacto c WHERE c.lugLugar = :idlugar";
		TypedQuery<LugContacto> q = em.createQuery(consulta, LugContacto.class);
		return q.setParameter("idlugar", idLugar).getResultList();
	}

	public List<LugLugar> buscarLugarPorNombre(String nombre) {
		String consulta = "SELECT l FROM LugLugar l WHERE l.nombre = :nombre";
		TypedQuery<LugLugar> q = em.createQuery(consulta, LugLugar.class);
		return q.setParameter("nombre", nombre).getResultList();
	}
}
