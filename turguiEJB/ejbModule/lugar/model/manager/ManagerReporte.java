package lugar.model.manager;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import turgui.model.entities.LugEstadoReporte;
import turgui.model.entities.LugLugar;
import turgui.model.entities.LugReporte;
import turgui.model.entities.UsuUsuario;

/**
 * Session Bean implementation class ManagerReporte
 */
@Stateless
@LocalBean
public class ManagerReporte {
	@PersistenceContext
	private EntityManager em;
	
    /**
     * Default constructor. 
     */
    public ManagerReporte() {
        // TODO Auto-generated constructor stub
    }
    
    public List<LugReporte> consultarReportes() {
    	TypedQuery<LugReporte> q = em.createNamedQuery("LugReporte.findAll", LugReporte.class);
    	return q.getResultList();
    }
    
    public void ingresarReporte(String observacion, LugEstadoReporte estadoReporte, UsuUsuario usuario, LugLugar lugar) {
    	LugReporte reporte = new LugReporte();
    	reporte.setObservacion(observacion);
    	reporte.setFechaCreacion(new Timestamp((new Date()).getTime()));
    	// aquí no va la fecha de modificación
    	reporte.setLugEstadoReporte(estadoReporte);
    	reporte.setUsuUsuario(usuario);
    	reporte.setLugLugar(lugar);
    	//
    	em.persist(reporte);
    }
    
    public void eliminarReporte(int idReporte) {
    	LugReporte reporte = buscarReportePorId(idReporte);
    	if (reporte != null) {
    		em.remove(reporte);
    	}
    }
    
    public void actualizarReporte(int idReporte, String observacion, LugEstadoReporte estadoReporte, UsuUsuario usuario, LugLugar lugar) throws Exception {
    	LugReporte reporte = buscarReportePorId(idReporte);
    	if (reporte == null) {
    		throw new Exception("No existe el reporte");
    	}
    	reporte.setObservacion(observacion);
    	reporte.setLugEstadoReporte(estadoReporte);
    	reporte.setUsuUsuario(usuario);
    	reporte.setLugLugar(lugar);
    	// aquí no se toca la fecha de creación
    	reporte.setFechaModificacion(new Timestamp((new Date()).getTime()));
    	//
    	em.merge(reporte);
    }
    
    public LugReporte buscarReportePorId(int idReporte) {
    	return em.find(LugReporte.class, idReporte);
    }

}
