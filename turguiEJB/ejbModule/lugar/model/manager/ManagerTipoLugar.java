package lugar.model.manager;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import turgui.model.entities.LugTipoLugar;

/**
 * Session Bean implementation class ManagerTipoLugar
 */
@Stateless
@LocalBean
public class ManagerTipoLugar {
	@PersistenceContext
	private EntityManager em;
	
    /**
     * Default constructor. 
     */
    public ManagerTipoLugar() {
        // TODO Auto-generated constructor stub
    }
    
    public List<LugTipoLugar> consultarTipoLugar() {
    	TypedQuery<LugTipoLugar> q = em.createNamedQuery("LugTipoLugar.findAll", LugTipoLugar.class); 
    	return q.getResultList();
    }
    public int findNumeroTipoLugares() {
    	TypedQuery<LugTipoLugar> q = em.createNamedQuery("LugTipoLugar.findAll", LugTipoLugar.class); 
    	return q.getResultList().size();
    }
    
    public void ingresarTipoLugar(LugTipoLugar tipoLugar) throws Exception {
    	if (existeNombre(tipoLugar)) {
    		throw new Exception("Ya existe el Tipo de Lugar.");
    	}
    	em.persist(tipoLugar);
    }
    
    public void eliminarTipoLugar(int idTipoLugar) {
    	LugTipoLugar tipoLugar = buscarTipoLugarPorId(idTipoLugar);
    	if (tipoLugar != null) {
    		em.remove(tipoLugar);
    	}
    }
    
    public void actualizarTipoLugar(LugTipoLugar tipoLugar) throws Exception {
    	LugTipoLugar viejoTipoLugar = buscarTipoLugarPorId(tipoLugar.getIdTipoLugar());
    	if (viejoTipoLugar == null) {
    		throw new Exception("No existe el Tipo de Lugar.");
    	}
    	viejoTipoLugar.setNombre(tipoLugar.getNombre());
    	viejoTipoLugar.setDescripcion(tipoLugar.getDescripcion());
    	em.merge(viejoTipoLugar);
    }
    
    public LugTipoLugar buscarTipoLugarPorId(int idTipoLugar) {
    	return em.find(LugTipoLugar.class, idTipoLugar);
    }
    
    private boolean existeNombre(LugTipoLugar tipoLugar) {
    	List<LugTipoLugar> listaTipoLugar = buscarTipoLugarPorNombre(tipoLugar.getNombre());
    	return listaTipoLugar.size() > 0;
    }
    
    public List<LugTipoLugar> buscarTipoLugarPorNombre(String nombre) {
    	String consulta = "SELECT t FROM LugTipoLugar t WHERE t.nombre = :nombre";
    	TypedQuery<LugTipoLugar> q = em.createQuery(consulta, LugTipoLugar.class);
    	return q.setParameter("nombre", nombre).getResultList();
    }
}
