package multimedia.model.manager;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import lugar.model.manager.ManagerLugar;
import turgui.model.entities.LugLugar;
import turgui.model.entities.MulCalificacion;
import turgui.model.entities.UsuUsuario;
import usuario.model.manager.ManagerUsuario;

/**
 * Session Bean implementation class ManagerCalificacion
 */
@Stateless
@LocalBean
public class ManagerCalificacion {

	/**
	 * Default constructor.
	 */
	@PersistenceContext
	private EntityManager em;

	@EJB
	private ManagerLugar managerLugar;
	@EJB
	private ManagerUsuario managerUsuario;

	public ManagerCalificacion() {
		// TODO Auto-generated constructor stub
	}

	public List<MulCalificacion> findAllCalificacion() {
		return em.createNamedQuery("MulCalificacion.findAll").getResultList();
	}

	public MulCalificacion findCalificacionById(int idCalificacion) {
		return em.find(MulCalificacion.class, idCalificacion);
	}

	public void insertCalificacion(MulCalificacion calificacion, int idLugar, int idUsuario) throws Exception {
		LugLugar lugar = managerLugar.buscarLugarPorId(idLugar);
		UsuUsuario usuario = managerUsuario.findUsuarioById(idUsuario);
		calificacion.setLugLugar(lugar);
		calificacion.setUsuUsuario(usuario);
		calificacion.setFechaCreacion(new Timestamp((new Date()).getTime()));
		calificacion.setFechaModificacion(new Timestamp((new Date()).getTime()));

		em.persist(calificacion);
	}

	public void deleteCalificacion(Integer idCalificacion) {
		MulCalificacion calificacion = findCalificacionById(idCalificacion);
		if (calificacion != null) {
			em.remove(calificacion);
		}
	}

	public void updateCalificacion(MulCalificacion calificacion, int idLugar, int idUsuario) throws Exception {
		MulCalificacion nueva_calificacion = findCalificacionById(calificacion.getIdCalificacion());
	
		if (nueva_calificacion == null) {
			throw new Exception("NO existe el tipo de archivo");
		} else {
			LugLugar lugar = managerLugar.buscarLugarPorId(idLugar);
			UsuUsuario usuario = managerUsuario.findUsuarioById(idUsuario);
			nueva_calificacion.setValor(calificacion.getValor());
			nueva_calificacion.setFechaCreacion(calificacion.getFechaCreacion());
			nueva_calificacion.setObservacion(calificacion.getObservacion());
			nueva_calificacion.setLugLugar(lugar);
			nueva_calificacion.setUsuUsuario(usuario);
			nueva_calificacion.setFechaModificacion(new Timestamp((new Date()).getTime()));
			em.merge(nueva_calificacion);
		}
	}

}
