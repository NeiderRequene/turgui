package multimedia.model.manager;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import informacion.model.manager.ManagerInformacion;
import lugar.model.manager.ManagerLugar;
import turgui.model.entities.LugLugar;
import turgui.model.entities.MulMultimedia;
import turgui.model.entities.MulTipoArchivo;

/**
 * Session Bean implementation class ManagerMultimedia
 */
@Stateless
@LocalBean
public class ManagerMultimedia {

	/**
	 * Default constructor.
	 */
	@PersistenceContext
	private EntityManager em;

	@EJB
	private ManagerTipoArchivo managerTipoArchivo;
	@EJB
	private ManagerLugar managerLugar;
	@EJB
	private ManagerInformacion managerInformacion;

	public ManagerMultimedia() {
		// TODO Auto-generated constructor stub
	}

	public List<MulMultimedia> findAllMultimedia() {
		return em.createNamedQuery("MulMultimedia.findAll").getResultList();
	}
	public List<MulMultimedia> findMultimediaByIdLugar(int idLugar) {
		String capsula = " m.lugLugar.idLugar = '" + idLugar + "'";
		String consulta = "SELECT m from MulMultimedia  m WHERE " + capsula;
		Query q = em.createQuery(consulta);
		return q.getResultList();
	}


	public MulMultimedia findMultimediaById(int idMultimedia) {
		return em.find(MulMultimedia.class, idMultimedia);
	}

	public List<LugLugar> findAllLugar() {
		return em.createNamedQuery("LugLugar.findAll").getResultList();
	}

	public LugLugar findLugarById(int idLugar) {
		return em.find(LugLugar.class, idLugar);
	}

	public void insertMultimedia(MulMultimedia multimedia, int idLugarMultimedia, int idTipoArchivoMultimedia,
			boolean numPortada, String extensioArchivo, byte[] contenidoArchivo) throws Exception {
		System.out.println("Extension" + extensioArchivo.split("/")[1]);
		extensioArchivo = extensioArchivo.split("/")[1];
		LugLugar lugar = managerLugar.buscarLugarPorId(idLugarMultimedia);
		MulTipoArchivo tipoArchivo = managerTipoArchivo.findTipoArchivoById(idTipoArchivoMultimedia);
		multimedia.setLugLugar(lugar);
		multimedia.setMulTipoArchivo(tipoArchivo);
		multimedia.setPortada(numPortada);
		String nombreArchivo = getFileName(extensioArchivo);
		String path = guardarArchivo(nombreArchivo, contenidoArchivo, tipoArchivo.getNombre());
		multimedia.setDireccion(path);
		multimedia.setFechaCreacion(new Timestamp((new Date()).getTime()));
		multimedia.setFechaModificacion(new Timestamp((new Date()).getTime()));
		em.persist(multimedia);

	}

	public String guardarArchivo(String nombreArchivo, byte[] contenido, String tipoArchivo) {

		try {
//			String directorio_actual =Paths.get(".").toAbsolutePath().normalize().toString();
			String directorio_raiz = managerInformacion.findInfParametroByNombre("carpeta_multimedia").get(0)
					.getValor();
			String directorio_tipoArchivo = directorio_raiz + tipoArchivo.toLowerCase();
			File file = new File(directorio_tipoArchivo);
			if (!file.exists()) {
				if (file.mkdir()) {
					System.out.println("Directory is created!");
				} else {
					System.out.println("Failed to create directory!");
				}
			}
			String ruta_completa = directorio_tipoArchivo + "/" + nombreArchivo;
			Path path = Paths.get(ruta_completa);
			byte[] strToBytes = contenido;
			Files.write(path, strToBytes);
			System.out.println("Archivo guardado" + ruta_completa);
			return ruta_completa;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Tuvimos un problema");
			return null;
		}
	}

	public void deleteMultimedia(Integer idMultimedia) {
		MulMultimedia multimedia = findMultimediaById(idMultimedia);
		if (multimedia != null) {
			em.remove(multimedia);
		}
	}

	public void updateMultimedia(MulMultimedia multimedia) throws Exception {
		MulMultimedia nueva_multimedia = findMultimediaById(multimedia.getIdMultimedia());
		if (nueva_multimedia == null) {
			throw new Exception("NO existe el tipo de archivo");
		} else {
			nueva_multimedia.setNombre(multimedia.getNombre());
			nueva_multimedia.setDescripcion(multimedia.getDescripcion());
			nueva_multimedia.setDireccion(multimedia.getDireccion());
			nueva_multimedia.setFechaModificacion(new Timestamp(new Date().getTime()));
			nueva_multimedia.setMulTipoArchivo(
					managerTipoArchivo.findTipoArchivoById(multimedia.getMulTipoArchivo().getIdTipoArchivo()));
			nueva_multimedia.setLugLugar(findLugarById(multimedia.getLugLugar().getIdLugar()));
			em.merge(nueva_multimedia);
		}
	}

	// Generating Unique File Name
	public String getFileName(String extensioArchivo) {
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss").format(new Date());
		return timeStamp + "_." + extensioArchivo;
	}
}
