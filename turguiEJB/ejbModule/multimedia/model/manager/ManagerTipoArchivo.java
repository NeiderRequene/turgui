package multimedia.model.manager;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import turgui.model.entities.MulTipoArchivo;

/**
 * Session Bean implementation class ManagerTipoArchivo
 */
@Stateless
@LocalBean
public class ManagerTipoArchivo {

	@PersistenceContext
	private EntityManager em;

	/**
	 * Default constructor.
	 */

	public ManagerTipoArchivo() {
		// TODO Auto-generated constructor stub
	}

	public List<MulTipoArchivo> findAllTipoArchivo() {
		return em.createNamedQuery("MulTipoArchivo.findAll").getResultList();
	}
	public int findNumeroTipoArchivo() {
		return em.createNamedQuery("MulTipoArchivo.findAll").getResultList().size();
	}

	public MulTipoArchivo findTipoArchivoById(int idTipoArchivo) {
		return em.find(MulTipoArchivo.class, idTipoArchivo);
	}

	public List<MulTipoArchivo> findTipoArchivoByNombre(String nombreTipoArchivo) {
		String capsula = " m.nombre = '" + nombreTipoArchivo + "'";
		String consulta = "SELECT m from MulTipoArchivo  m WHERE " + capsula;
		Query q = em.createQuery(consulta);
		return q.getResultList();
	}

	public void insertTipoArchivo(MulTipoArchivo tipoArchivo) throws Exception {

		if (findTipoArchivoByNombre(tipoArchivo.getNombre()).size() > 0) {
			throw new Exception("Ya existe el tipo de archivo");
		} else {
			em.persist(tipoArchivo);
		}
	}

	public void deleteTipoArchivo(Integer idTipoArchivo) {
		MulTipoArchivo tipoArchivo = findTipoArchivoById(idTipoArchivo);
		if (tipoArchivo != null) {
			em.remove(tipoArchivo);
		}
	}

	public void updateTipoArchivo(MulTipoArchivo tipoArchivo) throws Exception {
		MulTipoArchivo nuevo_tipoArchivo = findTipoArchivoById(tipoArchivo.getIdTipoArchivo());
		if (nuevo_tipoArchivo == null) {
			throw new Exception("NO existe el tipo de archivo");
		} else {
			nuevo_tipoArchivo.setNombre(tipoArchivo.getNombre());
			nuevo_tipoArchivo.setExtension(tipoArchivo.getExtension());
			em.merge(nuevo_tipoArchivo);
		}
	}

}
