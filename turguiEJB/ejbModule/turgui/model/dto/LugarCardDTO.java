package turgui.model.dto;

import java.sql.Timestamp;

public class LugarCardDTO {
	private Integer idLugar;
	private String descripcion;
	private String descripcionCorta;
	private Timestamp fechaCreacion;
	private Timestamp fechaModificacion;
	private String nombre;
	private Integer prioridad;
	// nuevos
	private String imagenPortada;
	private Integer calificacionPromedio;

	//
	public Integer getIdLugar() {
		return idLugar;
	}

	public void setIdLugar(Integer idLugar) {
		this.idLugar = idLugar;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcionCorta() {
		return descripcionCorta;
	}

	public void setDescripcionCorta(String descripcionCorta) {
		this.descripcionCorta = descripcionCorta;
	}

	public Timestamp getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Timestamp fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Timestamp getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(Timestamp fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getPrioridad() {
		return prioridad;
	}

	public void setPrioridad(Integer prioridad) {
		this.prioridad = prioridad;
	}

	public String getImagenPortada() {
		return imagenPortada;
	}

	public void setImagenPortada(String imagenPortada) {
		this.imagenPortada = imagenPortada;
	}

	public Integer getCalificacionPromedio() {
		return calificacionPromedio;
	}

	public void setCalificacionPromedio(Integer calificacionPromedio) {
		this.calificacionPromedio = calificacionPromedio;
	}
}