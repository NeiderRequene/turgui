package turgui.model.dto;

import java.sql.Timestamp;
import java.util.List;

import turgui.model.entities.InfDivisionPolitica;
import turgui.model.entities.LugTipoLugar;

public class LugarDTO {
	private Integer idLugar;
	private String codigoPostal;
	private String descripcion;
	private String descripcionCorta;
	private String direccion;
	private Timestamp fechaCreacion;
	private Timestamp fechaModificacion;
	private String latitud;
	private String longitud;
	private String nombre;
	private Integer prioridad;
	private String referencia;
	private List<ContactoDTO> lugContactos;
	private InfDivisionPolitica infDivisionPolitica;
	private LugTipoLugar lugTipoLugar;
	private List<CalificacionDTO> mulCalificacions;
	private List<MultimediaDTO> mulMultimedias;

	public Integer getIdLugar() {
		return idLugar;
	}

	public void setIdLugar(Integer idLugar) {
		this.idLugar = idLugar;
	}

	public String getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcionCorta() {
		return descripcionCorta;
	}

	public void setDescripcionCorta(String descripcionCorta) {
		this.descripcionCorta = descripcionCorta;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public Timestamp getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Timestamp fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Timestamp getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(Timestamp fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public String getLatitud() {
		return latitud;
	}

	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}

	public String getLongitud() {
		return longitud;
	}

	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getPrioridad() {
		return prioridad;
	}

	public void setPrioridad(Integer prioridad) {
		this.prioridad = prioridad;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public List<ContactoDTO> getLugContactos() {
		return lugContactos;
	}

	public void setLugContactos(List<ContactoDTO> lugContactos) {
		this.lugContactos = lugContactos;
	}

	public InfDivisionPolitica getInfDivisionPolitica() {
		return infDivisionPolitica;
	}

	public void setInfDivisionPolitica(InfDivisionPolitica infDivisionPolitica) {
		this.infDivisionPolitica = infDivisionPolitica;
	}

	public LugTipoLugar getLugTipoLugar() {
		return lugTipoLugar;
	}

	public void setLugTipoLugar(LugTipoLugar lugTipoLugar) {
		this.lugTipoLugar = lugTipoLugar;
	}

	public List<CalificacionDTO> getMulCalificacions() {
		return mulCalificacions;
	}

	public void setMulCalificacions(List<CalificacionDTO> mulCalificacions) {
		this.mulCalificacions = mulCalificacions;
	}

	public List<MultimediaDTO> getMulMultimedias() {
		return mulMultimedias;
	}

	public void setMulMultimedias(List<MultimediaDTO> mulMultimedias) {
		this.mulMultimedias = mulMultimedias;
	}

}
