package turgui.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the inf_division_politica database table.
 * 
 */
@Entity
@Table(name="inf_division_politica")
@NamedQuery(name="InfDivisionPolitica.findAll", query="SELECT i FROM InfDivisionPolitica i")
public class InfDivisionPolitica implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_division_politica", unique=true, nullable=false)
	private Integer idDivisionPolitica;

	@Column(nullable=false, length=100)
	private String nombre;

	//bi-directional many-to-one association to InfDivisionPolitica
	@ManyToOne
	@JoinColumn(name="id_division_politica_fk")
	private InfDivisionPolitica infDivisionPolitica;

	//bi-directional many-to-one association to InfDivisionPolitica
	@OneToMany(mappedBy="infDivisionPolitica")
	private List<InfDivisionPolitica> infDivisionPoliticas;

	//bi-directional many-to-one association to InfNivel
	@ManyToOne
	@JoinColumn(name="id_nivel", nullable=false)
	private InfNivel infNivel;

	//bi-directional many-to-one association to LugLugar
	@OneToMany(mappedBy="infDivisionPolitica")
	private List<LugLugar> lugLugars;

	public InfDivisionPolitica() {
	}

	public Integer getIdDivisionPolitica() {
		return this.idDivisionPolitica;
	}

	public void setIdDivisionPolitica(Integer idDivisionPolitica) {
		this.idDivisionPolitica = idDivisionPolitica;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public InfDivisionPolitica getInfDivisionPolitica() {
		return this.infDivisionPolitica;
	}

	public void setInfDivisionPolitica(InfDivisionPolitica infDivisionPolitica) {
		this.infDivisionPolitica = infDivisionPolitica;
	}

	public List<InfDivisionPolitica> getInfDivisionPoliticas() {
		return this.infDivisionPoliticas;
	}

	public void setInfDivisionPoliticas(List<InfDivisionPolitica> infDivisionPoliticas) {
		this.infDivisionPoliticas = infDivisionPoliticas;
	}

	public InfDivisionPolitica addInfDivisionPolitica(InfDivisionPolitica infDivisionPolitica) {
		getInfDivisionPoliticas().add(infDivisionPolitica);
		infDivisionPolitica.setInfDivisionPolitica(this);

		return infDivisionPolitica;
	}

	public InfDivisionPolitica removeInfDivisionPolitica(InfDivisionPolitica infDivisionPolitica) {
		getInfDivisionPoliticas().remove(infDivisionPolitica);
		infDivisionPolitica.setInfDivisionPolitica(null);

		return infDivisionPolitica;
	}

	public InfNivel getInfNivel() {
		return this.infNivel;
	}

	public void setInfNivel(InfNivel infNivel) {
		this.infNivel = infNivel;
	}

	public List<LugLugar> getLugLugars() {
		return this.lugLugars;
	}

	public void setLugLugars(List<LugLugar> lugLugars) {
		this.lugLugars = lugLugars;
	}

	public LugLugar addLugLugar(LugLugar lugLugar) {
		getLugLugars().add(lugLugar);
		lugLugar.setInfDivisionPolitica(this);

		return lugLugar;
	}

	public LugLugar removeLugLugar(LugLugar lugLugar) {
		getLugLugars().remove(lugLugar);
		lugLugar.setInfDivisionPolitica(null);

		return lugLugar;
	}

}