package turgui.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the inf_nivel database table.
 * 
 */
@Entity
@Table(name="inf_nivel")
@NamedQuery(name="InfNivel.findAll", query="SELECT i FROM InfNivel i")
public class InfNivel implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_nivel", unique=true, nullable=false)
	private Integer idNivel;

	@Column(length=100)
	private String nombre;

	//bi-directional many-to-one association to InfDivisionPolitica
	@OneToMany(mappedBy="infNivel")
	private List<InfDivisionPolitica> infDivisionPoliticas;

	public InfNivel() {
	}

	public Integer getIdNivel() {
		return this.idNivel;
	}

	public void setIdNivel(Integer idNivel) {
		this.idNivel = idNivel;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<InfDivisionPolitica> getInfDivisionPoliticas() {
		return this.infDivisionPoliticas;
	}

	public void setInfDivisionPoliticas(List<InfDivisionPolitica> infDivisionPoliticas) {
		this.infDivisionPoliticas = infDivisionPoliticas;
	}

	public InfDivisionPolitica addInfDivisionPolitica(InfDivisionPolitica infDivisionPolitica) {
		getInfDivisionPoliticas().add(infDivisionPolitica);
		infDivisionPolitica.setInfNivel(this);

		return infDivisionPolitica;
	}

	public InfDivisionPolitica removeInfDivisionPolitica(InfDivisionPolitica infDivisionPolitica) {
		getInfDivisionPoliticas().remove(infDivisionPolitica);
		infDivisionPolitica.setInfNivel(null);

		return infDivisionPolitica;
	}

}