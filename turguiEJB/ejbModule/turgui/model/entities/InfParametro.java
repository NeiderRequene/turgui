package turgui.model.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the inf_parametro database table.
 * 
 */
@Entity
@Table(name="inf_parametro")
@NamedQuery(name="InfParametro.findAll", query="SELECT i FROM InfParametro i")
public class InfParametro implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_parametro", unique=true, nullable=false)
	private Integer idParametro;

	@Column(nullable=false, length=100)
	private String nombre;

	@Column(length=2147483647)
	private String valor;

	public InfParametro() {
	}

	public Integer getIdParametro() {
		return this.idParametro;
	}

	public void setIdParametro(Integer idParametro) {
		this.idParametro = idParametro;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getValor() {
		return this.valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

}