package turgui.model.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the lug_contacto database table.
 * 
 */
@Entity
@Table(name="lug_contacto")
@NamedQuery(name="LugContacto.findAll", query="SELECT l FROM LugContacto l")
public class LugContacto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_contacto", unique=true, nullable=false)
	private Integer idContacto;

	@Column(length=20)
	private String celular;

	@Column(length=200)
	private String correo;

	@Column(nullable=false, length=50)
	private String nombre;

	@Column(length=15)
	private String telefono;

	//bi-directional many-to-one association to LugLugar
	@ManyToOne
	@JoinColumn(name="id_lugar", nullable=false)
	private LugLugar lugLugar;

	public LugContacto() {
	}

	public Integer getIdContacto() {
		return this.idContacto;
	}

	public void setIdContacto(Integer idContacto) {
		this.idContacto = idContacto;
	}

	public String getCelular() {
		return this.celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getCorreo() {
		return this.correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTelefono() {
		return this.telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public LugLugar getLugLugar() {
		return this.lugLugar;
	}

	public void setLugLugar(LugLugar lugLugar) {
		this.lugLugar = lugLugar;
	}

}