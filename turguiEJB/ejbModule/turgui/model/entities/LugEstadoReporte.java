package turgui.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the lug_estado_reporte database table.
 * 
 */
@Entity
@Table(name="lug_estado_reporte")
@NamedQuery(name="LugEstadoReporte.findAll", query="SELECT l FROM LugEstadoReporte l")
public class LugEstadoReporte implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_estado_reporte", unique=true, nullable=false)
	private Integer idEstadoReporte;

	@Column(length=20)
	private String nombre;

	//bi-directional many-to-one association to LugReporte
	@OneToMany(mappedBy="lugEstadoReporte")
	private List<LugReporte> lugReportes;

	public LugEstadoReporte() {
	}

	public Integer getIdEstadoReporte() {
		return this.idEstadoReporte;
	}

	public void setIdEstadoReporte(Integer idEstadoReporte) {
		this.idEstadoReporte = idEstadoReporte;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<LugReporte> getLugReportes() {
		return this.lugReportes;
	}

	public void setLugReportes(List<LugReporte> lugReportes) {
		this.lugReportes = lugReportes;
	}

	public LugReporte addLugReporte(LugReporte lugReporte) {
		getLugReportes().add(lugReporte);
		lugReporte.setLugEstadoReporte(this);

		return lugReporte;
	}

	public LugReporte removeLugReporte(LugReporte lugReporte) {
		getLugReportes().remove(lugReporte);
		lugReporte.setLugEstadoReporte(null);

		return lugReporte;
	}

}