package turgui.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the lug_lugar database table.
 * 
 */
@Entity
@Table(name="lug_lugar")
@NamedQuery(name="LugLugar.findAll", query="SELECT l FROM LugLugar l")
public class LugLugar implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_lugar", unique=true, nullable=false)
	private Integer idLugar;

	@Column(name="codigo_postal", length=20)
	private String codigoPostal;

	@Column(length=2147483647)
	private String descripcion;

	@Column(name="descripcion_corta", length=250)
	private String descripcionCorta;

	@Column(length=100)
	private String direccion;

	@Column(name="fecha_creacion")
	private Timestamp fechaCreacion;

	@Column(name="fecha_modificacion")
	private Timestamp fechaModificacion;

	@Column(nullable=false, length=30)
	private String latitud;

	@Column(nullable=false, length=30)
	private String longitud;

	@Column(nullable=false, length=500)
	private String nombre;

	private Integer prioridad;

	@Column(length=100)
	private String referencia;

	//bi-directional many-to-one association to LugContacto
	@OneToMany(mappedBy="lugLugar")
	private List<LugContacto> lugContactos;

	//bi-directional many-to-one association to InfDivisionPolitica
	@ManyToOne
	@JoinColumn(name="id_division_politica", nullable=false)
	private InfDivisionPolitica infDivisionPolitica;

	//bi-directional many-to-one association to LugTipoLugar
	@ManyToOne
	@JoinColumn(name="id_tipo_lugar", nullable=false)
	private LugTipoLugar lugTipoLugar;

	//bi-directional many-to-one association to UsuUsuario
	@ManyToOne
	@JoinColumn(name="id_usuario", nullable=false)
	private UsuUsuario usuUsuario;

	//bi-directional many-to-one association to LugReporte
	@OneToMany(mappedBy="lugLugar")
	private List<LugReporte> lugReportes;

	//bi-directional many-to-one association to MulCalificacion
	@OneToMany(mappedBy="lugLugar")
	private List<MulCalificacion> mulCalificacions;

	//bi-directional many-to-one association to MulMultimedia
	@OneToMany(mappedBy="lugLugar")
	private List<MulMultimedia> mulMultimedias;

	public LugLugar() {
	}

	public Integer getIdLugar() {
		return this.idLugar;
	}

	public void setIdLugar(Integer idLugar) {
		this.idLugar = idLugar;
	}

	public String getCodigoPostal() {
		return this.codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcionCorta() {
		return this.descripcionCorta;
	}

	public void setDescripcionCorta(String descripcionCorta) {
		this.descripcionCorta = descripcionCorta;
	}

	public String getDireccion() {
		return this.direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public Timestamp getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Timestamp fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Timestamp getFechaModificacion() {
		return this.fechaModificacion;
	}

	public void setFechaModificacion(Timestamp fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public String getLatitud() {
		return this.latitud;
	}

	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}

	public String getLongitud() {
		return this.longitud;
	}

	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getPrioridad() {
		return this.prioridad;
	}

	public void setPrioridad(Integer prioridad) {
		this.prioridad = prioridad;
	}

	public String getReferencia() {
		return this.referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public List<LugContacto> getLugContactos() {
		return this.lugContactos;
	}

	public void setLugContactos(List<LugContacto> lugContactos) {
		this.lugContactos = lugContactos;
	}

	public LugContacto addLugContacto(LugContacto lugContacto) {
		getLugContactos().add(lugContacto);
		lugContacto.setLugLugar(this);

		return lugContacto;
	}

	public LugContacto removeLugContacto(LugContacto lugContacto) {
		getLugContactos().remove(lugContacto);
		lugContacto.setLugLugar(null);

		return lugContacto;
	}

	public InfDivisionPolitica getInfDivisionPolitica() {
		return this.infDivisionPolitica;
	}

	public void setInfDivisionPolitica(InfDivisionPolitica infDivisionPolitica) {
		this.infDivisionPolitica = infDivisionPolitica;
	}

	public LugTipoLugar getLugTipoLugar() {
		return this.lugTipoLugar;
	}

	public void setLugTipoLugar(LugTipoLugar lugTipoLugar) {
		this.lugTipoLugar = lugTipoLugar;
	}

	public UsuUsuario getUsuUsuario() {
		return this.usuUsuario;
	}

	public void setUsuUsuario(UsuUsuario usuUsuario) {
		this.usuUsuario = usuUsuario;
	}

	public List<LugReporte> getLugReportes() {
		return this.lugReportes;
	}

	public void setLugReportes(List<LugReporte> lugReportes) {
		this.lugReportes = lugReportes;
	}

	public LugReporte addLugReporte(LugReporte lugReporte) {
		getLugReportes().add(lugReporte);
		lugReporte.setLugLugar(this);

		return lugReporte;
	}

	public LugReporte removeLugReporte(LugReporte lugReporte) {
		getLugReportes().remove(lugReporte);
		lugReporte.setLugLugar(null);

		return lugReporte;
	}

	public List<MulCalificacion> getMulCalificacions() {
		return this.mulCalificacions;
	}

	public void setMulCalificacions(List<MulCalificacion> mulCalificacions) {
		this.mulCalificacions = mulCalificacions;
	}

	public MulCalificacion addMulCalificacion(MulCalificacion mulCalificacion) {
		getMulCalificacions().add(mulCalificacion);
		mulCalificacion.setLugLugar(this);

		return mulCalificacion;
	}

	public MulCalificacion removeMulCalificacion(MulCalificacion mulCalificacion) {
		getMulCalificacions().remove(mulCalificacion);
		mulCalificacion.setLugLugar(null);

		return mulCalificacion;
	}

	public List<MulMultimedia> getMulMultimedias() {
		return this.mulMultimedias;
	}

	public void setMulMultimedias(List<MulMultimedia> mulMultimedias) {
		this.mulMultimedias = mulMultimedias;
	}

	public MulMultimedia addMulMultimedia(MulMultimedia mulMultimedia) {
		getMulMultimedias().add(mulMultimedia);
		mulMultimedia.setLugLugar(this);

		return mulMultimedia;
	}

	public MulMultimedia removeMulMultimedia(MulMultimedia mulMultimedia) {
		getMulMultimedias().remove(mulMultimedia);
		mulMultimedia.setLugLugar(null);

		return mulMultimedia;
	}

}