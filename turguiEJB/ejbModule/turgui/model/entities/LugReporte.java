package turgui.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the lug_reporte database table.
 * 
 */
@Entity
@Table(name="lug_reporte")
@NamedQuery(name="LugReporte.findAll", query="SELECT l FROM LugReporte l")
public class LugReporte implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_reporte", unique=true, nullable=false)
	private Integer idReporte;

	@Column(name="fecha_creacion")
	private Timestamp fechaCreacion;

	@Column(name="fecha_modificacion")
	private Timestamp fechaModificacion;

	@Column(length=2147483647)
	private String observacion;

	//bi-directional many-to-one association to LugEstadoReporte
	@ManyToOne
	@JoinColumn(name="id_estado_reporte", nullable=false)
	private LugEstadoReporte lugEstadoReporte;

	//bi-directional many-to-one association to LugLugar
	@ManyToOne
	@JoinColumn(name="id_lugar", nullable=false)
	private LugLugar lugLugar;

	//bi-directional many-to-one association to UsuUsuario
	@ManyToOne
	@JoinColumn(name="id_usuario")
	private UsuUsuario usuUsuario;

	public LugReporte() {
	}

	public Integer getIdReporte() {
		return this.idReporte;
	}

	public void setIdReporte(Integer idReporte) {
		this.idReporte = idReporte;
	}

	public Timestamp getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Timestamp fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Timestamp getFechaModificacion() {
		return this.fechaModificacion;
	}

	public void setFechaModificacion(Timestamp fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public String getObservacion() {
		return this.observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public LugEstadoReporte getLugEstadoReporte() {
		return this.lugEstadoReporte;
	}

	public void setLugEstadoReporte(LugEstadoReporte lugEstadoReporte) {
		this.lugEstadoReporte = lugEstadoReporte;
	}

	public LugLugar getLugLugar() {
		return this.lugLugar;
	}

	public void setLugLugar(LugLugar lugLugar) {
		this.lugLugar = lugLugar;
	}

	public UsuUsuario getUsuUsuario() {
		return this.usuUsuario;
	}

	public void setUsuUsuario(UsuUsuario usuUsuario) {
		this.usuUsuario = usuUsuario;
	}

}