package turgui.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the lug_tipo_lugar database table.
 * 
 */
@Entity
@Table(name="lug_tipo_lugar")
@NamedQuery(name="LugTipoLugar.findAll", query="SELECT l FROM LugTipoLugar l")
public class LugTipoLugar implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_tipo_lugar", unique=true, nullable=false)
	private Integer idTipoLugar;

	@Column(nullable=false, length=100)
	private String descripcion;

	@Column(nullable=false, length=50)
	private String nombre;

	//bi-directional many-to-one association to LugLugar
	@OneToMany(mappedBy="lugTipoLugar")
	private List<LugLugar> lugLugars;

	public LugTipoLugar() {
	}

	public Integer getIdTipoLugar() {
		return this.idTipoLugar;
	}

	public void setIdTipoLugar(Integer idTipoLugar) {
		this.idTipoLugar = idTipoLugar;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<LugLugar> getLugLugars() {
		return this.lugLugars;
	}

	public void setLugLugars(List<LugLugar> lugLugars) {
		this.lugLugars = lugLugars;
	}

	public LugLugar addLugLugar(LugLugar lugLugar) {
		getLugLugars().add(lugLugar);
		lugLugar.setLugTipoLugar(this);

		return lugLugar;
	}

	public LugLugar removeLugLugar(LugLugar lugLugar) {
		getLugLugars().remove(lugLugar);
		lugLugar.setLugTipoLugar(null);

		return lugLugar;
	}

}