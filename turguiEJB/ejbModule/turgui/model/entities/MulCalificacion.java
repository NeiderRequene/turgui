package turgui.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the mul_calificacion database table.
 * 
 */
@Entity
@Table(name="mul_calificacion")
@NamedQuery(name="MulCalificacion.findAll", query="SELECT m FROM MulCalificacion m")
public class MulCalificacion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_calificacion", unique=true, nullable=false)
	private Integer idCalificacion;

	@Column(name="fecha_creacion")
	private Timestamp fechaCreacion;

	@Column(name="fecha_modificacion")
	private Timestamp fechaModificacion;

	@Column(length=200)
	private String observacion;

	@Column(nullable=false)
	private Long valor;

	//bi-directional many-to-one association to LugLugar
	@ManyToOne
	@JoinColumn(name="id_lugar", nullable=false)
	private LugLugar lugLugar;

	//bi-directional many-to-one association to UsuUsuario
	@ManyToOne
	@JoinColumn(name="id_usuario", nullable=false)
	private UsuUsuario usuUsuario;

	public MulCalificacion() {
	}

	public Integer getIdCalificacion() {
		return this.idCalificacion;
	}

	public void setIdCalificacion(Integer idCalificacion) {
		this.idCalificacion = idCalificacion;
	}

	public Timestamp getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Timestamp fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Timestamp getFechaModificacion() {
		return this.fechaModificacion;
	}

	public void setFechaModificacion(Timestamp fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public String getObservacion() {
		return this.observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public Long getValor() {
		return this.valor;
	}

	public void setValor(Long valor) {
		this.valor = valor;
	}

	public LugLugar getLugLugar() {
		return this.lugLugar;
	}

	public void setLugLugar(LugLugar lugLugar) {
		this.lugLugar = lugLugar;
	}

	public UsuUsuario getUsuUsuario() {
		return this.usuUsuario;
	}

	public void setUsuUsuario(UsuUsuario usuUsuario) {
		this.usuUsuario = usuUsuario;
	}

}