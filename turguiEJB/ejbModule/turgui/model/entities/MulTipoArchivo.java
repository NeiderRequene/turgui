package turgui.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the mul_tipo_archivo database table.
 * 
 */
@Entity
@Table(name="mul_tipo_archivo")
@NamedQuery(name="MulTipoArchivo.findAll", query="SELECT m FROM MulTipoArchivo m")
public class MulTipoArchivo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_tipo_archivo", unique=true, nullable=false)
	private Integer idTipoArchivo;

	@Column(length=100)
	private String extension;

	@Column(length=100)
	private String nombre;

	//bi-directional many-to-one association to MulMultimedia
	@OneToMany(mappedBy="mulTipoArchivo")
	private List<MulMultimedia> mulMultimedias;

	public MulTipoArchivo() {
	}

	public Integer getIdTipoArchivo() {
		return this.idTipoArchivo;
	}

	public void setIdTipoArchivo(Integer idTipoArchivo) {
		this.idTipoArchivo = idTipoArchivo;
	}

	public String getExtension() {
		return this.extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<MulMultimedia> getMulMultimedias() {
		return this.mulMultimedias;
	}

	public void setMulMultimedias(List<MulMultimedia> mulMultimedias) {
		this.mulMultimedias = mulMultimedias;
	}

	public MulMultimedia addMulMultimedia(MulMultimedia mulMultimedia) {
		getMulMultimedias().add(mulMultimedia);
		mulMultimedia.setMulTipoArchivo(this);

		return mulMultimedia;
	}

	public MulMultimedia removeMulMultimedia(MulMultimedia mulMultimedia) {
		getMulMultimedias().remove(mulMultimedia);
		mulMultimedia.setMulTipoArchivo(null);

		return mulMultimedia;
	}

}