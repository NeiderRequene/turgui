package turgui.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the usu_bitacora database table.
 * 
 */
@Entity
@Table(name="usu_bitacora")
@NamedQuery(name="UsuBitacora.findAll", query="SELECT u FROM UsuBitacora u")
public class UsuBitacora implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="codigo_evento", unique=true, nullable=false)
	private Integer codigoEvento;

	@Column(length=200)
	private String descripcion;

	@Column(name="direccion_ip", length=20)
	private String direccionIp;

	@Column(name="fecha_evento", nullable=false)
	private Timestamp fechaEvento;

	@Column(nullable=false, length=100)
	private String metodo;

	//bi-directional many-to-one association to UsuUsuario
	@ManyToOne
	@JoinColumn(name="id_usuario", nullable=false)
	private UsuUsuario usuUsuario;

	public UsuBitacora() {
	}

	public Integer getCodigoEvento() {
		return this.codigoEvento;
	}

	public void setCodigoEvento(Integer codigoEvento) {
		this.codigoEvento = codigoEvento;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDireccionIp() {
		return this.direccionIp;
	}

	public void setDireccionIp(String direccionIp) {
		this.direccionIp = direccionIp;
	}

	public Timestamp getFechaEvento() {
		return this.fechaEvento;
	}

	public void setFechaEvento(Timestamp fechaEvento) {
		this.fechaEvento = fechaEvento;
	}

	public String getMetodo() {
		return this.metodo;
	}

	public void setMetodo(String metodo) {
		this.metodo = metodo;
	}

	public UsuUsuario getUsuUsuario() {
		return this.usuUsuario;
	}

	public void setUsuUsuario(UsuUsuario usuUsuario) {
		this.usuUsuario = usuUsuario;
	}

}