package turgui.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the usu_nacionalidad database table.
 * 
 */
@Entity
@Table(name="usu_nacionalidad")
@NamedQuery(name="UsuNacionalidad.findAll", query="SELECT u FROM UsuNacionalidad u")
public class UsuNacionalidad implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_nacionalidad", unique=true, nullable=false)
	private Integer idNacionalidad;

	@Column(length=100)
	private String nombre;

	//bi-directional many-to-one association to UsuUsuario
	@OneToMany(mappedBy="usuNacionalidad")
	private List<UsuUsuario> usuUsuarios;

	public UsuNacionalidad() {
	}

	public Integer getIdNacionalidad() {
		return this.idNacionalidad;
	}

	public void setIdNacionalidad(Integer idNacionalidad) {
		this.idNacionalidad = idNacionalidad;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<UsuUsuario> getUsuUsuarios() {
		return this.usuUsuarios;
	}

	public void setUsuUsuarios(List<UsuUsuario> usuUsuarios) {
		this.usuUsuarios = usuUsuarios;
	}

	public UsuUsuario addUsuUsuario(UsuUsuario usuUsuario) {
		getUsuUsuarios().add(usuUsuario);
		usuUsuario.setUsuNacionalidad(this);

		return usuUsuario;
	}

	public UsuUsuario removeUsuUsuario(UsuUsuario usuUsuario) {
		getUsuUsuarios().remove(usuUsuario);
		usuUsuario.setUsuNacionalidad(null);

		return usuUsuario;
	}

}