package turgui.model.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the usu_usuario database table.
 * 
 */
@Entity
@Table(name="usu_usuario")
@NamedQuery(name="UsuUsuario.findAll", query="SELECT u FROM UsuUsuario u")
public class UsuUsuario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_usuario", unique=true, nullable=false)
	private Integer idUsuario;

	@Column(nullable=false, length=100)
	private String apellidos;

	@Column(name="confirmacion_correo")
	private Boolean confirmacionCorreo;

	@Column(nullable=false, length=100)
	private String contrasenia;

	@Column(nullable=false, length=100)
	private String correo;

	@Column(length=100)
	private String cuidad;

	@Column(name="estado_civil", length=20)
	private String estadoCivil;

	@Column(name="fecha_creacion")
	private Timestamp fechaCreacion;

	@Column(name="fecha_modificacion")
	private Timestamp fechaModificacion;

	@Column(name="fecha_nacimiento")
	private Time fechaNacimiento;

	@Column(nullable=false, length=100)
	private String nombres;

	@Column(name="pregunta_seguridad", length=2147483647)
	private String preguntaSeguridad;

	@Column(length=100)
	private String profesion;

	@Column(name="respuesta_seguridad", length=2147483647)
	private String respuestaSeguridad;

	@Column(length=20)
	private String sexo;

	//bi-directional many-to-one association to LugLugar
	@OneToMany(mappedBy="usuUsuario")
	private List<LugLugar> lugLugars;

	//bi-directional many-to-one association to LugReporte
	@OneToMany(mappedBy="usuUsuario")
	private List<LugReporte> lugReportes;

	//bi-directional many-to-one association to MulCalificacion
	@OneToMany(mappedBy="usuUsuario")
	private List<MulCalificacion> mulCalificacions;

	//bi-directional many-to-one association to UsuBitacora
	@OneToMany(mappedBy="usuUsuario")
	private List<UsuBitacora> usuBitacoras;

	//bi-directional many-to-one association to UsuUsuarioRol
	@OneToMany(mappedBy="usuUsuario")
	private List<UsuUsuarioRol> usuUsuarioRols;

	//bi-directional many-to-one association to UsuNacionalidad
	@ManyToOne
	@JoinColumn(name="id_nacionalidad")
	private UsuNacionalidad usuNacionalidad;

	public UsuUsuario() {
	}

	public Integer getIdUsuario() {
		return this.idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getApellidos() {
		return this.apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public Boolean getConfirmacionCorreo() {
		return this.confirmacionCorreo;
	}

	public void setConfirmacionCorreo(Boolean confirmacionCorreo) {
		this.confirmacionCorreo = confirmacionCorreo;
	}

	public String getContrasenia() {
		return this.contrasenia;
	}

	public void setContrasenia(String contrasenia) {
		this.contrasenia = contrasenia;
	}

	public String getCorreo() {
		return this.correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getCuidad() {
		return this.cuidad;
	}

	public void setCuidad(String cuidad) {
		this.cuidad = cuidad;
	}

	public String getEstadoCivil() {
		return this.estadoCivil;
	}

	public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public Timestamp getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Timestamp fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Timestamp getFechaModificacion() {
		return this.fechaModificacion;
	}

	public void setFechaModificacion(Timestamp fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public Time getFechaNacimiento() {
		return this.fechaNacimiento;
	}

	public void setFechaNacimiento(Time fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getNombres() {
		return this.nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getPreguntaSeguridad() {
		return this.preguntaSeguridad;
	}

	public void setPreguntaSeguridad(String preguntaSeguridad) {
		this.preguntaSeguridad = preguntaSeguridad;
	}

	public String getProfesion() {
		return this.profesion;
	}

	public void setProfesion(String profesion) {
		this.profesion = profesion;
	}

	public String getRespuestaSeguridad() {
		return this.respuestaSeguridad;
	}

	public void setRespuestaSeguridad(String respuestaSeguridad) {
		this.respuestaSeguridad = respuestaSeguridad;
	}

	public String getSexo() {
		return this.sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public List<LugLugar> getLugLugars() {
		return this.lugLugars;
	}

	public void setLugLugars(List<LugLugar> lugLugars) {
		this.lugLugars = lugLugars;
	}

	public LugLugar addLugLugar(LugLugar lugLugar) {
		getLugLugars().add(lugLugar);
		lugLugar.setUsuUsuario(this);

		return lugLugar;
	}

	public LugLugar removeLugLugar(LugLugar lugLugar) {
		getLugLugars().remove(lugLugar);
		lugLugar.setUsuUsuario(null);

		return lugLugar;
	}

	public List<LugReporte> getLugReportes() {
		return this.lugReportes;
	}

	public void setLugReportes(List<LugReporte> lugReportes) {
		this.lugReportes = lugReportes;
	}

	public LugReporte addLugReporte(LugReporte lugReporte) {
		getLugReportes().add(lugReporte);
		lugReporte.setUsuUsuario(this);

		return lugReporte;
	}

	public LugReporte removeLugReporte(LugReporte lugReporte) {
		getLugReportes().remove(lugReporte);
		lugReporte.setUsuUsuario(null);

		return lugReporte;
	}

	public List<MulCalificacion> getMulCalificacions() {
		return this.mulCalificacions;
	}

	public void setMulCalificacions(List<MulCalificacion> mulCalificacions) {
		this.mulCalificacions = mulCalificacions;
	}

	public MulCalificacion addMulCalificacion(MulCalificacion mulCalificacion) {
		getMulCalificacions().add(mulCalificacion);
		mulCalificacion.setUsuUsuario(this);

		return mulCalificacion;
	}

	public MulCalificacion removeMulCalificacion(MulCalificacion mulCalificacion) {
		getMulCalificacions().remove(mulCalificacion);
		mulCalificacion.setUsuUsuario(null);

		return mulCalificacion;
	}

	public List<UsuBitacora> getUsuBitacoras() {
		return this.usuBitacoras;
	}

	public void setUsuBitacoras(List<UsuBitacora> usuBitacoras) {
		this.usuBitacoras = usuBitacoras;
	}

	public UsuBitacora addUsuBitacora(UsuBitacora usuBitacora) {
		getUsuBitacoras().add(usuBitacora);
		usuBitacora.setUsuUsuario(this);

		return usuBitacora;
	}

	public UsuBitacora removeUsuBitacora(UsuBitacora usuBitacora) {
		getUsuBitacoras().remove(usuBitacora);
		usuBitacora.setUsuUsuario(null);

		return usuBitacora;
	}

	public List<UsuUsuarioRol> getUsuUsuarioRols() {
		return this.usuUsuarioRols;
	}

	public void setUsuUsuarioRols(List<UsuUsuarioRol> usuUsuarioRols) {
		this.usuUsuarioRols = usuUsuarioRols;
	}

	public UsuUsuarioRol addUsuUsuarioRol(UsuUsuarioRol usuUsuarioRol) {
		getUsuUsuarioRols().add(usuUsuarioRol);
		usuUsuarioRol.setUsuUsuario(this);

		return usuUsuarioRol;
	}

	public UsuUsuarioRol removeUsuUsuarioRol(UsuUsuarioRol usuUsuarioRol) {
		getUsuUsuarioRols().remove(usuUsuarioRol);
		usuUsuarioRol.setUsuUsuario(null);

		return usuUsuarioRol;
	}

	public UsuNacionalidad getUsuNacionalidad() {
		return this.usuNacionalidad;
	}

	public void setUsuNacionalidad(UsuNacionalidad usuNacionalidad) {
		this.usuNacionalidad = usuNacionalidad;
	}

}