package usuario.model.manager;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import turgui.model.entities.UsuBitacora;
import turgui.model.entities.UsuUsuario;

/**
 * Session Bean implementation class ManagerAuditoria
 */
@Stateless
@LocalBean
public class ManagerAuditoria {

	@EJB
	private ManagerUsuario managerUsuario;
	
	@PersistenceContext
	private EntityManager em;

	public ManagerAuditoria() {
		// TODO Auto-generated constructor stub
	}

	public void crearEventoBitacora(String correo, Class clase, String metodo, String descripcion, String direccion_ip) throws Exception {

		UsuBitacora bitacora = new UsuBitacora();

		if (correo == null || correo.length() == 0)
			throw new Exception("Error auditoria: debe indicar el correo del usuario.");
		if (metodo == null || metodo.length() == 0)
			throw new Exception("Error auditoria: debe indicar el metodo que genera el evento.");

		List<UsuUsuario> usuario = managerUsuario.finUduarioByCorreo(correo);
		if (usuario.size() == 0) {
			throw new Exception("No existe el usuario especificado.");
		}

		UsuUsuario usuario_ingreso = usuario.get(0);

		bitacora.setUsuUsuario(usuario_ingreso);
		bitacora.setDescripcion(descripcion);
		bitacora.setDireccionIp(direccion_ip);
		bitacora.setFechaEvento(new Timestamp((new Date()).getTime()));
		bitacora.setMetodo(clase.getSimpleName()+"/"+metodo);	
		
		em.persist(bitacora);
	}
	
	public List<UsuBitacora> findAllBitacoras(){
		String consulta = "SELECT u FROM UsuBitacora u";
		Query q = em.createQuery(consulta, UsuBitacora.class); //UsuBitacora.class, consulta
		return q.getResultList();
	}
	
}
