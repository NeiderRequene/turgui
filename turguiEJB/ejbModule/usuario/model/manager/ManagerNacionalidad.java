package usuario.model.manager;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import turgui.model.entities.UsuNacionalidad;
import turgui.model.entities.UsuUsuario;

/**
 * Session Bean implementation class ManagerNacionalidad
 */
@Stateless
@LocalBean
public class ManagerNacionalidad {

	@PersistenceContext
	private EntityManager em;

	public ManagerNacionalidad() {
		// TODO Auto-generated constructor stub
	}

	public List<UsuNacionalidad> findAllNacionalidad() {
		String consulta = "SELECT u FROM UsuNacionalidad u";
		Query q = em.createQuery(consulta, UsuNacionalidad.class);

		return q.getResultList();
	}

	public UsuNacionalidad findUsuNacionalidadById(int idNacionalidad) {
		return em.find(UsuNacionalidad.class, idNacionalidad);
	}

	public List<UsuNacionalidad> finUsuNacionalidadBynombre(String nombre) {
		String capsula = " u.nombre = '" + nombre + "'";
		String consulta = "SELECT u FROM UsuNacionalidad u WHERE " + capsula;
		Query q = em.createQuery(consulta);

		return q.getResultList();
	}

	public void insertarNacionalidad(UsuNacionalidad nacionalidad) throws Exception {
		if (finUsuNacionalidadBynombre(nacionalidad.getNombre()).size() > 0) {
			throw new Exception("Ya existe el nacionalidad");
		}
		em.persist(nacionalidad);
	}

	public void eliminarNacionalidad(int idNacionalidad) {
		UsuNacionalidad nacionalidad = em.find(UsuNacionalidad.class, idNacionalidad);
		if (nacionalidad != null) {
			em.remove(nacionalidad);
		}
	}

	public void actualizarNacionalidad(UsuNacionalidad nacionalidad) throws Exception {
		UsuNacionalidad n = findUsuNacionalidadById(nacionalidad.getIdNacionalidad());

		if (n == null) {
			throw new Exception("No existe nacionalidad");
		}

		n.setNombre(nacionalidad.getNombre());

		em.merge(n);
	}

}
