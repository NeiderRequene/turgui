package usuario.model.manager;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import turgui.model.entities.UsuRol;
import turgui.model.entities.UsuUsuario;


@Stateless
@LocalBean
public class ManagerRol {
	
	@PersistenceContext
	private EntityManager em;

    public ManagerRol() {
        // TODO Auto-generated constructor stub
    }
    
    
    public  List<UsuRol> findAllRoles(){
    	String consulta = "SELECT u FROM UsuRol u";
    	Query q = em.createQuery(consulta, UsuRol.class);
    	return q.getResultList();
    }
    
	public UsuRol findRolById(int idRol) {
		return em.find(UsuRol.class, idRol);
	}
	
	public List<UsuRol> finRolByNombre(String nombre){
		String capsula = " u.nombre = '" + nombre + "'";
		String consulta = "SELECT u FROM UsuRol u WHERE " + capsula;
		Query q = em.createQuery(consulta);
		
		return q.getResultList();
	}
	
	public void insertarRol(UsuRol rol) throws Exception {
		if(finRolByNombre(rol.getNombre()).size() > 0) {  // rol.getNombre() .size()
			throw new Exception("Ya existe el rol");
		}
		
		em.persist(rol);
	}
	
	public void eliminarRol(int idRol) {
		UsuRol rol = em.find(UsuRol.class, idRol);
		if(rol != null) {
			em.remove(rol);
		}		
	}
	
	public void actualizarRol(UsuRol rol) throws Exception{
		UsuRol r = findRolById(rol.getIdRol());
		
		if(r == null) {
			throw new Exception("No existe el rol");
		}
		
		r.setNombre(rol.getNombre());
		
		em.merge(r);
		
		
	}
    

}
