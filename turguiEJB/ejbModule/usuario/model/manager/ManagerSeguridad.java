package usuario.model.manager;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import turgui.model.dto.LoginDTO;
import turgui.model.entities.UsuNacionalidad;
import turgui.model.entities.UsuUsuario;
import turgui.model.entities.UsuUsuarioRol;

/**
 * Session Bean implementation class ManagerSeguridad
 */
@Stateless
@LocalBean
public class ManagerSeguridad {
	@PersistenceContext
	private EntityManager em;

	@EJB
	private ManagerUsuario managerUsuario;

	/**
	 * Default constructor.
	 */
	public ManagerSeguridad() {
		// TODO Auto-generated constructor stub
	}

	public LoginDTO iniciarSesion(String correo, String contrasenia, String rol) throws Exception {
		List<UsuUsuario> usuario = managerUsuario.finUduarioByCorreo(correo);

		if (usuario.size() == 0) {
			throw new Exception("No existe el usuario especificado.");
		}
		UsuUsuario usuario_ingreso = usuario.get(0);

		if (usuario_ingreso == null) {
			throw new Exception("Error en usuario y/o clave.");
		}

		// Contrasenia encriptada
		contrasenia = encriptarContrasenia(contrasenia);
		System.out.println("---------------------- Login: " + contrasenia + " " + usuario_ingreso.getContrasenia()
				+ " ---------------------------");
		if (!usuario_ingreso.getContrasenia().equals(contrasenia)) {
			throw new Exception("Error! Datos incorrectos");
		}

		List<UsuUsuarioRol> usurol = usuario_ingreso.getUsuUsuarioRols();
		List<String> listaRol = new ArrayList();
		for (UsuUsuarioRol usuUsuarioRol : usurol) {
			listaRol.add(usuUsuarioRol.getUsuRol().getNombre());
		}

		LoginDTO loginDTO = new LoginDTO();
		if (listaRol.contains(rol)) {
			loginDTO.setRol(rol);
		} else {
			throw new Exception("Este usuario no tiene los permisos suficientes para acceder como " + rol);
		}
		loginDTO.setCorreo(usuario_ingreso.getCorreo());
		if (rol.equals("usuario")) {
			loginDTO.setRutaAcceso("/faces/usuario/index.xhtml");
		} else if (rol.equals("administrador")) {
			loginDTO.setRutaAcceso("/faces/administrador/index.xhtml");
		} else if (rol.equals("superadministrador")) {
			loginDTO.setRutaAcceso("/faces/superadministrador/index.xhtml");
		}

		loginDTO.setUsuario(usuario_ingreso.getNombres());
		return loginDTO;
	}

	public boolean verificarPreguntaSeguridad(String correo, String pregunta, String respuesta) throws Exception {

		List<UsuUsuario> usuario = managerUsuario.finUduarioByCorreo(correo);
		if (usuario.size() == 0) {
			throw new Exception("No existe el usuario especificado.");
		}

		UsuUsuario usuario_ingreso = usuario.get(0);

		pregunta = pregunta.toUpperCase();
		respuesta = respuesta.toUpperCase();

		if (usuario_ingreso.getRespuestaSeguridad().toUpperCase().equals(respuesta)) {
			return true;
		}

		return false;
	}

	public String consultarPregunta(String correo) {

		System.out.println("Usuario: " + correo);
		List<UsuUsuario> usuario = managerUsuario.finUduarioByCorreo(correo);

		UsuUsuario usuario_ingreso = usuario.get(0);

		if (usuario_ingreso == null) {
			return "";
		}

		System.out.println("Error:" + usuario_ingreso.getApellidos());

		return usuario_ingreso.getPreguntaSeguridad();
	}

	public void cambiarContrasena(String correo, String nuevaContrasena) {

		System.out.println("Usuario: " + correo);
		List<UsuUsuario> usuario = managerUsuario.finUduarioByCorreo(correo);

		UsuUsuario usuario_ingreso = usuario.get(0);

		nuevaContrasena = encriptarContrasenia(nuevaContrasena);
		usuario_ingreso.setContrasenia(nuevaContrasena);
		em.merge(usuario_ingreso);

	}

	public String encriptarContrasenia(String password) {
		byte[] newPassword = null;
		try {
			newPassword = MessageDigest.getInstance("SHA").digest(password.getBytes("UTF-8"));
		} catch (Exception e) {

		}

		String encriptado = Base64.getEncoder().encodeToString(newPassword);
		return encriptado;
	}

}
