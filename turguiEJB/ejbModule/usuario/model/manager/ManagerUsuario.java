package usuario.model.manager;

import java.security.MessageDigest;
import java.sql.Timestamp;
import java.util.Base64;
import java.util.Date;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import turgui.model.entities.UsuNacionalidad;
import turgui.model.entities.UsuRol;
import turgui.model.entities.UsuUsuario;
import turgui.model.entities.UsuUsuarioRol;

@Stateless
@LocalBean
public class ManagerUsuario {

	@PersistenceContext
	private EntityManager em;

	public ManagerUsuario() {
	}

	public List<UsuUsuario> findAllUsuarios() {
		String consulta = "SELECT u FROM UsuUsuario u";
		Query q = em.createQuery(consulta, UsuUsuario.class);

		return q.getResultList();
	}
	public int findNumeroUsuarios() {
		String consulta = "SELECT u FROM UsuUsuario u";
		Query q = em.createQuery(consulta, UsuUsuario.class);
		return q.getResultList().size();
	}

	public UsuUsuario findUsuarioById(int idUsuario) {
		return em.find(UsuUsuario.class, idUsuario);
	}
	
	
	public List<UsuUsuario> finUduarioByCorreo(String correo){
		String capsula = " u.correo = '" + correo + "'";
		String consulta = "SELECT u FROM UsuUsuario u WHERE " + capsula;
		Query q = em.createQuery(consulta);
		
		return q.getResultList();
	}

	public void insertarUsuario(UsuUsuario usuario, int id_nacionalidad) throws Exception {
		if (finUduarioByCorreo(usuario.getCorreo()).size() > 0) { // usuario.getCorreo() .size()
			throw new Exception("Ya existe el usuario");
		}
		
		UsuNacionalidad nacionalidad = em.find(UsuNacionalidad.class, id_nacionalidad);
		
		usuario.setUsuNacionalidad(nacionalidad);
		usuario.setContrasenia(encriptarContrasenia(usuario.getContrasenia()));
		usuario.setFechaCreacion(new Timestamp((new Date()).getTime()));
		usuario.setFechaModificacion(new Timestamp((new Date()).getTime()));
		usuario.setConfirmacionCorreo(false);

		em.persist(usuario);
	}
	
	
	public void registrarUsuario(UsuUsuario usuario, int id_nacionalidad) throws Exception {
		if (finUduarioByCorreo(usuario.getCorreo()).size() > 0) { // usuario.getCorreo() .size()
			throw new Exception("Ya existe el usuario");
		}
		
		UsuNacionalidad nacionalidad = em.find(UsuNacionalidad.class, id_nacionalidad);
		UsuRol rol = em.find(UsuRol.class, 3);
		
		usuario.setUsuNacionalidad(nacionalidad);
		usuario.setContrasenia(encriptarContrasenia(usuario.getContrasenia()));
		usuario.setFechaCreacion(new Timestamp((new Date()).getTime()));
		usuario.setFechaModificacion(new Timestamp((new Date()).getTime()));
		usuario.setConfirmacionCorreo(false);

		UsuUsuarioRol usurol = new UsuUsuarioRol();
		usurol.setUsuUsuario(usuario);
		usurol.setUsuRol(rol);
		
		em.persist(usuario);
		em.persist(usurol);
	}

	public void eliminarUsuario(int idUsuario) {
		UsuUsuario usuario = em.find(UsuUsuario.class, idUsuario);
		if (usuario != null) {
			em.remove(usuario);
		}
	}
	
	public void actualizarUsuario(UsuUsuario usuario) throws Exception{
		UsuUsuario u = findUsuarioById(usuario.getIdUsuario());
		
		if(u == null) {
			throw new Exception("No existe el usuario");
		}
		
		u.setApellidos(usuario.getApellidos());
		u.setConfirmacionCorreo(usuario.getConfirmacionCorreo());
		u.setContrasenia(encriptarContrasenia(usuario.getContrasenia()));
		u.setCorreo(usuario.getCorreo());
		u.setNombres(usuario.getNombres());
		
		em.merge(u);
	}
	
	
	public String encriptarContrasenia(String password) {
		byte[] newPassword = null;
		try {
			newPassword = MessageDigest.getInstance("SHA").digest(password.getBytes("UTF-8"));
		} catch (Exception e) {
			
		}
		
		String encriptado = Base64.getEncoder().encodeToString(newPassword);
		return encriptado;
	}
	
	

}
