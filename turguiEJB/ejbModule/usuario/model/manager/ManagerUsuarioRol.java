package usuario.model.manager;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import turgui.model.entities.UsuRol;
import turgui.model.entities.UsuUsuario;
import turgui.model.entities.UsuUsuarioRol;

/**
 * Session Bean implementation class ManagerUsuarioRol
 */
@Stateless
@LocalBean
public class ManagerUsuarioRol {

	@PersistenceContext
	private EntityManager em;

	public ManagerUsuarioRol() {
	}

	public List<UsuUsuarioRol> findAllUsuarioRoles() {
		String consulta = "SELECT u FROM UsuUsuarioRol u";
		Query q = em.createNamedQuery("UsuUsuarioRol.findAll");
		return q.getResultList();
	}

	public List<UsuUsuarioRol> findUsuRolByIds(int idRol, int idUsuario) {
		String capsula = " uu.idRol = " + idRol + " AND uuu.idUsuario = " + idUsuario;
		String consulta = "SELECT u, uu, uuu FROM UsuUsuarioRol u, UsuRol uu, UsuUsuario uuu WHERE " + capsula;
		Query q = em.createQuery(consulta);
		return q.getResultList();
	}

	public void insertarUsuarioRol( int idRol, int idUsuario) throws Exception {
//		if (findUsuRolByIds(idRol, idUsuario).size() > 0) {
//			throw new Exception("No existe esa asignacion");
//		}
		UsuUsuarioRol usurol = new UsuUsuarioRol();
		
		UsuUsuario usuario = em.find(UsuUsuario.class, idUsuario); //.class idUsuario
		usurol.setUsuUsuario(usuario);
		
		UsuRol rol = em.find(UsuRol.class, idRol);
		usurol.setUsuRol(rol);
		
		try {
			em.persist(usurol);
		} catch (Exception e) {
			// TODO: handle exception
			throw new Exception("No se puede ingresar esa asignacion");
		}
		

	}

	public void eliminarUsuarioRol(UsuUsuarioRol usurol) throws Exception {
		System.out.println(usurol.getIdUsuarioRol()); //usurol.get
		
		//UsuUsuarioRol ur = em.find(UsuUsuarioRol.class, usurol.getIdUsuarioRol());
		if (usurol != null) {
			em.remove(usurol);
		}
	}
	
	
	public List<UsuUsuarioRol> findUsuarioRolByNombres(UsuUsuarioRol usurol){ // .getNombre()  .getNombres
		
		
		return null;
	}

	public void actualizarUsuarioRol(UsuUsuarioRol usurol, int idRol, int idUsuario) throws Exception {
		if (findUsuRolByIds(idRol, idUsuario).size() == 0) {
			throw new Exception("No existe esa asignacion");
		}
		
		UsuUsuario usu = em.find(UsuUsuario.class, idUsuario);
		UsuRol rol = em.find(UsuRol.class, idRol);
		
		usurol.setUsuUsuario(usu);
		usurol.setUsuRol(rol);

		em.merge(usurol);

	}
	
	
	public void actualizarUsuarioRol() {
		
	}

}
