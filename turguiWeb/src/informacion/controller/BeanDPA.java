package informacion.controller;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.model.SelectItem;
import javax.inject.Named;

import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import informacion.model.manager.ManagerDPA;
import multimedia.controller.JSFUtil;
import turgui.model.entities.InfDivisionPolitica;
import turgui.model.entities.InfNivel;
import turgui.model.entities.InfParametro;

@Named
@SessionScoped

public class BeanDPA implements Serializable {

	private static final long serialVersionUID = 1L;
	private List<InfDivisionPolitica> listaProvincias;
	private List<InfDivisionPolitica> listaCantones;
	private List<InfDivisionPolitica> listaParroquias;

	private List<InfNivel> listaNiveles;
	private int idNivelSeleccionado;
	private int idProvinciaSeleccionado;
	private int idCantonSeleccionado;

	private String nombreActualizar;
	private boolean esCanton;
	private boolean esParroquia;
	private String NombreNuevo;
	private TreeNode arbolDatos;
	
	private InfDivisionPolitica nueva_division_politica;
	private InfDivisionPolitica dpa_selecionado;
	
	@EJB
	private ManagerDPA managerDpa;

	@PostConstruct
	public void inicializar() {
		listaNiveles = managerDpa.findAllLevels();
		esCanton= false;
		esParroquia = false;
		arbolDatos = crearArbolDatos();
		setNueva_division_politica(new InfDivisionPolitica());
	}
	
	/*
	 * Método para crear el arbol de datos del DPA
	 * */
	public TreeNode crearArbolDatos() {
		List<InfDivisionPolitica> listaDivision_nivel01 = managerDpa.findAllByNIvel(1);
		arbolDatos = new DefaultTreeNode("Raiz", null);
		for (InfDivisionPolitica dpa_nivel01 : listaDivision_nivel01) {
			TreeNode nivel01 = new DefaultTreeNode(dpa_nivel01, arbolDatos);
			List<InfDivisionPolitica> listaDivision_nivel02 = managerDpa
					.findInfDivisonPoliticaByIdNivelAndIdDivisionPolitica(2, dpa_nivel01.getIdDivisionPolitica());
			for (InfDivisionPolitica dpa_nivel02 : listaDivision_nivel02) {
				TreeNode nivel02 = new DefaultTreeNode(dpa_nivel02, nivel01);
				
				List<InfDivisionPolitica> listaDivision_nivel03 = managerDpa
						.findInfDivisonPoliticaByIdNivelAndIdDivisionPolitica(3, dpa_nivel02.getIdDivisionPolitica());
				for (InfDivisionPolitica dpa_nivel03 : listaDivision_nivel03) {
					TreeNode nivel03 = new DefaultTreeNode(dpa_nivel03, nivel02);
				}
			}
		}
		return arbolDatos;
	}

	public void actionListenerMostrarDpaNombresNiveles() {
		if(idNivelSeleccionado ==1) {
			esCanton = false;
			esParroquia= false;
		}else if(idNivelSeleccionado == 2){
			listaProvincias = managerDpa.findAllByNIvel(1);
			esCanton = true;
			esParroquia= false;
		}else {
			listaProvincias = managerDpa.findAllByNIvel(1);
			listaCantones = managerDpa.findInfDivisonPoliticaByIdNivelAndIdDivisionPolitica(2, idProvinciaSeleccionado);
			esCanton = true;
			esParroquia= true;
		}
	}

	public void acctionInsertDPA() {
		try {
			// managerDpa.insertDPA(DPA);
			int idDpaSeleccionado=0;
			if(idNivelSeleccionado==2) {
				idDpaSeleccionado= idProvinciaSeleccionado;
			}else if(idNivelSeleccionado == 3) {
				idDpaSeleccionado = idCantonSeleccionado;
			}
			managerDpa.insertDPA(NombreNuevo, idNivelSeleccionado, idDpaSeleccionado);
			crearArbolDatos();
			NombreNuevo="";
			JSFUtil.crearMensajeInfo("Datos de DPA Ingresado");

		} catch (Exception e) {
			// TODO: handle exception
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void actionEliminarDPA() {
		System.out.println("Eliminar");
		boolean resultado =managerDpa.deleteDpa(dpa_selecionado.getIdDivisionPolitica());
		if(resultado) {
			dpa_selecionado=new InfDivisionPolitica();
			crearArbolDatos();
			JSFUtil.crearMensajeInfo("Datos de DPA eliminado correctamente");
		}else {
			JSFUtil.crearMensajeError("NO se puedo eliminar porque tiene nodos hijos");
		}
	
	}
	public void actionListenerActualizarDPA() throws Exception {
		System.out.println("Editar");
		managerDpa.updateInfDPA(dpa_selecionado);
		dpa_selecionado=new InfDivisionPolitica();
		crearArbolDatos();
	}



	public List<InfNivel> getListaNiveles() {
		return listaNiveles;
	}

	public void setListaNiveles(List<InfNivel> listaNiveles) {
		this.listaNiveles = listaNiveles;
	}

	public int getIdNivelSeleccionado() {
		return idNivelSeleccionado;
	}

	public void setIdNivelSeleccionado(int idNivelSeleccionado) {
		this.idNivelSeleccionado = idNivelSeleccionado;
	}

	public String getNombreNuevo() {
		return NombreNuevo;
	}

	public void setNombreNuevo(String nombreNuevo) {
		NombreNuevo = nombreNuevo;
	}

	public String getNombreActualizar() {
		return nombreActualizar;
	}

	public void setNombreActualizar(String nombreActualizar) {
		this.nombreActualizar = nombreActualizar;
	}

	
	public TreeNode getArbolDatos() {
		return arbolDatos;
	}
	public void setArbolDatos(TreeNode arbolDatos) {
		this.arbolDatos = arbolDatos;
	}
	public List<InfDivisionPolitica> getListaProvincias() {
		return listaProvincias;
	}
	public void setListaProvincias(List<InfDivisionPolitica> listaProvincias) {
		this.listaProvincias = listaProvincias;
	}
	public List<InfDivisionPolitica> getListaCantones() {
		return listaCantones;
	}
	public void setListaCantones(List<InfDivisionPolitica> listaCantones) {
		this.listaCantones = listaCantones;
	}
	public List<InfDivisionPolitica> getListaParroquias() {
		return listaParroquias;
	}
	public void setListaParroquias(List<InfDivisionPolitica> listaParroquias) {
		this.listaParroquias = listaParroquias;
	}

	public int getIdProvinciaSeleccionado() {
		return idProvinciaSeleccionado;
	}

	public void setIdProvinciaSeleccionado(int idProvinciaSeleccionado) {
		this.idProvinciaSeleccionado = idProvinciaSeleccionado;
	}

	public int getIdCantonSeleccionado() {
		return idCantonSeleccionado;
	}

	public void setIdCantonSeleccionado(int idCantonSeleccionado) {
		this.idCantonSeleccionado = idCantonSeleccionado;
	}


	public boolean isEsCanton() {
		return esCanton;
	}

	public void setEsCanton(boolean esCanton) {
		this.esCanton = esCanton;
	}

	public boolean isEsParroquia() {
		return esParroquia;
	}

	public void setEsParroquia(boolean esParroquia) {
		this.esParroquia = esParroquia;
	}

	public InfDivisionPolitica getNueva_division_politica() {
		return nueva_division_politica;
	}

	public void setNueva_division_politica(InfDivisionPolitica nueva_division_politica) {
		this.nueva_division_politica = nueva_division_politica;
	}

	public InfDivisionPolitica getDpa_selecionado() {
		return dpa_selecionado;
	}

	public void setDpa_selecionado(InfDivisionPolitica dpa_selecionado) {
		this.dpa_selecionado = dpa_selecionado;
	}
	

}
