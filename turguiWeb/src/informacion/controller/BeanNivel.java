package informacion.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.inject.Named;

import com.sun.org.apache.xalan.internal.xsltc.compiler.Pattern;

import informacion.model.manager.ManagerInformacion;
import informacion.model.manager.ManagerNivel;
import multimedia.controller.JSFUtil;
import turgui.model.entities.InfNivel;
import turgui.model.entities.InfParametro;

@Named
@SessionScoped

public class BeanNivel implements Serializable {

	private static final long serialVersionUID = 1L;

	private InfNivel infNivel;
	private List<InfNivel> listaInfNivel;

	@EJB
	private ManagerNivel managerNivel;

	@PostConstruct
	public void inicializar() {
		listaInfNivel = managerNivel.findAllInfnivels();
		infNivel = new InfNivel();
	}

	public void accionInsertNivel() {
		try {

			managerNivel.insertNivel(infNivel);
			listaInfNivel = managerNivel.findAllInfnivels();
			infNivel = new InfNivel();
			JSFUtil.crearMensajeInfo("Datos de Nivel Ingresado");

		} catch (Exception e) {
			// TODO: handle exception
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}

	}

	public void actionListenerEliminarNivel() {
		try {

			managerNivel.deleteNivel(infNivel.getIdNivel());
			listaInfNivel = managerNivel.findAllInfnivels();

			infNivel = new InfNivel();

			JSFUtil.crearMensajeInfo("Nivel eliminado correctamente");

		} catch (Exception e) {
			// TODO: handle exception
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListenerActualizarNivel() {
		try {
			managerNivel.updateInfNivel(infNivel);
			listaInfNivel = managerNivel.findAllInfnivels();

			infNivel = new InfNivel();
			JSFUtil.crearMensajeInfo("Parametro actualizado correctamente");

		} catch (Exception e) {
			// TODO: handle exception
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}

	public void cambiarDatosNivel(InfNivel nuevo_nivel) {
		infNivel.setNombre(nuevo_nivel.getNombre());
		infNivel.setIdNivel(nuevo_nivel.getIdNivel());

	}

	

	public InfNivel getInfNivel() {
		return infNivel;
	}

	public void setInfNivel(InfNivel infNivel) {
		this.infNivel = infNivel;
	}

	public List<InfNivel> getListaInfNivel() {
		return listaInfNivel;
	}

	public void setListaInfNivel(List<InfNivel> listaInfNivel) {
		this.listaInfNivel = listaInfNivel;
	}

}
