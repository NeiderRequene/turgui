package informacion.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import informacion.model.manager.ManagerInformacion;
import multimedia.controller.JSFUtil;
import turgui.model.entities.InfParametro;


@Named
@SessionScoped
public class BeanParametro implements Serializable{

	private static final long serialVersionUID = 1L;
	private InfParametro infParametro;
	private List<InfParametro> listaInfParametro;
	
	@EJB
	private ManagerInformacion managerInformacion;
	
	@PostConstruct
	public void inicializar() {
		listaInfParametro = managerInformacion.findAllInfParametros();
		infParametro = new InfParametro();
	}
	public void accionInsertParametro() {
		try {
			managerInformacion.insertParametro(infParametro);
			listaInfParametro = managerInformacion.findAllInfParametros();
			infParametro = new InfParametro();
			JSFUtil.crearMensajeInfo("Datos de Parametro Ingresado");

		} catch (Exception e) {
			// TODO: handle exception
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}

	}

	public void actionListenerEliminarParametro() {
		try {
			managerInformacion.deleteParametro(infParametro.getIdParametro());
			listaInfParametro = managerInformacion.findAllInfParametros();

			infParametro = new InfParametro();

			JSFUtil.crearMensajeInfo("TParametro eliminado correctamente");

		} catch (Exception e) {
			// TODO: handle exception
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}
	public void actionListenerActualizarParametro() {
		try {
			managerInformacion.updateInfParametro(infParametro);
			listaInfParametro = managerInformacion.findAllInfParametros();
			infParametro = new InfParametro();
			JSFUtil.crearMensajeInfo("Parametro actualizado correctamente");

		} catch (Exception e) {
			// TODO: handle exception
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void cambiarDatosParametro(InfParametro nuevo_parametro) {
		infParametro.setIdParametro(nuevo_parametro.getIdParametro());
		infParametro.setNombre(nuevo_parametro.getNombre());
		infParametro.setValor(nuevo_parametro.getValor());
		System.out.println("Cambiando: "+infParametro.getNombre());
		
	}
	public InfParametro getInfParametro() {
		return infParametro;
	}
	public void setInfParametro(InfParametro infParametro) {
		this.infParametro = infParametro;
	}
	public List<InfParametro> getListaInfParametro() {
		return listaInfParametro;
	}
	public void setListaInfParametro(List<InfParametro> listaInfParametro) {
		this.listaInfParametro = listaInfParametro;
	}

}
