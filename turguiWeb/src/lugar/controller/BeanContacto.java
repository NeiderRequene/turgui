package lugar.controller;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import lugar.model.manager.ManagerContacto;
import lugar.model.manager.ManagerLugar;
import multimedia.controller.JSFUtil;
import turgui.model.entities.LugContacto;
import turgui.model.entities.LugLugar;

import java.io.Serializable;
import java.util.List;

@Named
@SessionScoped
public class BeanContacto implements Serializable {
	private static final long serialVersionUID = 1L;
	private List<LugContacto> listaContactos;
	private List<LugLugar> listaLugares;
	private LugContacto contacto;
	private int idLugar;
	@EJB
	private ManagerContacto managerContacto;
	@EJB
	private ManagerLugar managerLugar;

	@PostConstruct
	public void inicializar() {
		listaContactos = managerContacto.consutarContactos();
		listaLugares = managerLugar.consultarLugar();
		contacto = new LugContacto();
	}

	public void actionIngresarContacto() {
		try {
			managerContacto.ingresarContacto(managerLugar.buscarLugarPorId(idLugar), contacto.getNombre(),
					contacto.getTelefono(), contacto.getCelular(), contacto.getCorreo());
			listaContactos = managerContacto.consutarContactos();
			contacto = new LugContacto();
			JSFUtil.crearMensajeInfo("Contacto creado existosamente");
		} catch (Exception e) {
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListenerActualizarContacto() {
		try {
			managerContacto.actualizarContacto(contacto.getIdContacto(), contacto.getNombre(), contacto.getTelefono(),
					contacto.getCelular(), contacto.getCorreo(), managerLugar.buscarLugarPorId(idLugar));
			listaContactos = managerContacto.consutarContactos();
			JSFUtil.crearMensajeInfo("El contacto " + contacto.getNombre() + " ha sido actualizado correctamente.");
			contacto = new LugContacto();
		} catch (Exception e) {
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListenerEliminarContacto() {
		try {
			managerContacto.eliminarContacto(contacto.getIdContacto());
			listaContactos = managerContacto.consutarContactos();
			JSFUtil.crearMensajeInfo("Contacto eliminado correctamente");
			contacto = new LugContacto();
		} catch (Exception e) {
			JSFUtil.crearMensajeError("Hubo problemas eliminando el contacto");
			e.printStackTrace();
		}
	}

	public void actionListenerCambiarContacto(LugContacto contacto) {
		this.contacto.setIdContacto(contacto.getIdContacto());
		this.contacto.setNombre(contacto.getNombre());
		this.contacto.setTelefono(contacto.getTelefono());
		this.contacto.setCelular(contacto.getCelular());
		this.contacto.setCorreo(contacto.getCorreo());
		this.contacto.setLugLugar(contacto.getLugLugar());
	}

	public List<LugContacto> getListaContactos() {
		return listaContactos;
	}

	public void setListaContactos(List<LugContacto> listaContactos) {
		this.listaContactos = listaContactos;
	}

	public List<LugLugar> getListaLugares() {
		return listaLugares;
	}

	public void setListaLugares(List<LugLugar> listaLugares) {
		this.listaLugares = listaLugares;
	}

	public LugContacto getContacto() {
		return contacto;
	}

	public void setContacto(LugContacto contacto) {
		this.contacto = contacto;
	}

	public int getIdLugar() {
		return idLugar;
	}

	public void setIdLugar(int idLugar) {
		this.idLugar = idLugar;
	}

}
