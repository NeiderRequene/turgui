package lugar.controller;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import lugar.model.manager.ManagerEstadoReporte;
import multimedia.controller.JSFUtil;
import turgui.model.entities.LugEstadoReporte;

import java.io.Serializable;
import java.util.List;

@Named
@SessionScoped
public class BeanEstadoReporte implements Serializable {
	private static final long serialVersionUID = 1L;
	private List<LugEstadoReporte> listaEstadoReporte;
	private LugEstadoReporte estadoReporte;
	@EJB
	private ManagerEstadoReporte managerEstadoReporte;

	@PostConstruct
	public void inicializar() {
		listaEstadoReporte = managerEstadoReporte.consultarEstadoReporte();
		estadoReporte = new LugEstadoReporte();
	}

	public void actionListenerIngresarEstadoReporte() {
		try {
			managerEstadoReporte.ingresarEstadoReporte(estadoReporte.getNombre());
			listaEstadoReporte = managerEstadoReporte.consultarEstadoReporte();
			estadoReporte = new LugEstadoReporte();
			JSFUtil.crearMensajeInfo("Estado Reporte creado existosamente");
		} catch (Exception e) {
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListenerActualizarEstadoReporte() {
		try {
			managerEstadoReporte.actualizarEstadoReporte(estadoReporte);
			listaEstadoReporte = managerEstadoReporte.consultarEstadoReporte();
			JSFUtil.crearMensajeInfo(
					"El Estado Reporte " + estadoReporte.getNombre() + " ha sido actualizado correctamente.");
			estadoReporte = new LugEstadoReporte();
		} catch (Exception e) {
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListenerEliminarEstadoReporte() {
		try {
			managerEstadoReporte.eliminarEstadoReporte(estadoReporte.getIdEstadoReporte());
			listaEstadoReporte = managerEstadoReporte.consultarEstadoReporte();
			estadoReporte = new LugEstadoReporte();
			JSFUtil.crearMensajeInfo("Estado Reporte eliminado correctamente");
		} catch (Exception e) {
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListenerCambiarEstadoReporte(LugEstadoReporte reporte) {
		estadoReporte.setIdEstadoReporte(reporte.getIdEstadoReporte());
		estadoReporte.setNombre(reporte.getNombre());
	}

	public List<LugEstadoReporte> getListaEstadoReporte() {
		return listaEstadoReporte;
	}

	public void setListaEstadoReporte(List<LugEstadoReporte> listaEstadoReporte) {
		this.listaEstadoReporte = listaEstadoReporte;
	}

	public LugEstadoReporte getEstadoReporte() {
		return estadoReporte;
	}

	public void setEstadoReporte(LugEstadoReporte estadoReporte) {
		this.estadoReporte = estadoReporte;
	}

}
