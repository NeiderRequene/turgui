package lugar.controller;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import org.primefaces.event.map.PointSelectEvent;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;

import informacion.model.manager.ManagerDPA;
import lugar.model.manager.ManagerLugar;
import lugar.model.manager.ManagerTipoLugar;
import multimedia.controller.JSFUtil;
import turgui.model.entities.InfDivisionPolitica;
import turgui.model.entities.LugLugar;
import usuario.model.manager.ManagerUsuario;

import java.io.Serializable;
import java.util.List;

@Named
@SessionScoped
public class BeanLugar implements Serializable {
	private static final long serialVersionUID = 1L;
	private List<LugLugar> listaLugar;
	private LugLugar lugar;
	private int idUsuario;
	private int idTipoLugar;
	private int idProvincia;
	private int idCanton;
	private int idParroquia;
	private List<InfDivisionPolitica> listaProvincias;
	private List<InfDivisionPolitica> listaCantones;
	private List<InfDivisionPolitica> listaParroquias;
	private MapModel modelMapa;
	@EJB
	private ManagerLugar managerLugar;
	@EJB
	private ManagerUsuario managerUsuario;
	@EJB
	private ManagerTipoLugar managerTipoLugar;
	@EJB
	private ManagerDPA managerDpa;

	@PostConstruct
	public void inicializar() {
		setListaLugar(managerLugar.consultarLugar());
		lugar = new LugLugar();
		lugar.setInfDivisionPolitica(new InfDivisionPolitica());
		// dpa
		listaProvincias = managerDpa.findAllByNIvel(1);
		modelMapa = new DefaultMapModel();
	}

	public void seleccionPuntoMapa(PointSelectEvent event) {
		LatLng latlng = event.getLatLng();
		System.out.println("Longitud: " + latlng.getLat());
		System.out.println("Latitud: " + latlng.getLng());
		lugar.setLatitud(String.valueOf(latlng.getLat()));
		lugar.setLongitud(String.valueOf(latlng.getLng()));
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
				"Point Selected", "Lat:" + latlng.getLat() + ", Lng:" + latlng.getLng()));
	}

	public String actionIngresarLugar() {
		try {
			lugar.setUsuUsuario(managerUsuario.findUsuarioById(idUsuario));
			lugar.setLugTipoLugar(managerTipoLugar.buscarTipoLugarPorId(idTipoLugar));
			lugar.setInfDivisionPolitica(managerDpa.findInfDivisionById(idParroquia));
			managerLugar.ingresarLugar(lugar.getNombre(), lugar.getDireccion(), lugar.getReferencia(),
					lugar.getDescripcion(), lugar.getDescripcionCorta(), lugar.getLatitud(), lugar.getLongitud(),
					lugar.getCodigoPostal(), lugar.getPrioridad(), lugar.getUsuUsuario(), lugar.getLugTipoLugar(),
					lugar.getInfDivisionPolitica());
			listaLugar = managerLugar.consultarLugar();
			lugar = new LugLugar();
			JSFUtil.crearMensajeInfo("Lugar creado existosamente");
			return "lugarlistar";
		} catch (Exception e) {
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
			return "";
		}
	}

	public String actionActualizarLugar() {
		try {
			managerLugar.actualizarLugar(lugar);
			listaLugar = managerLugar.consultarLugar();
			JSFUtil.crearMensajeInfo("El Lugar " + lugar.getNombre() + " ha sido actualizado correctamente.");
			lugar = new LugLugar();
			return "lugarlistar";
		} catch (Exception e) {
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
			return "";
		}

	}

	public void actionListenerEliminarLugar() {
		try {
			managerLugar.eliminarLugar(lugar.getIdLugar());
			listaLugar = managerLugar.consultarLugar();
			JSFUtil.crearMensajeInfo("Lugar eliminado correctamente");
			lugar = new LugLugar();
		} catch (Exception e) {
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListenerCambiarLugar(LugLugar lugLugar) {
		lugar.setIdLugar(lugLugar.getIdLugar());
		lugar.setNombre(lugLugar.getNombre());
		lugar.setDireccion(lugLugar.getDireccion());
		lugar.setReferencia(lugLugar.getReferencia());
		lugar.setDescripcion(lugLugar.getDescripcion());
		lugar.setDescripcionCorta(lugLugar.getDescripcionCorta());
		lugar.setLatitud(lugLugar.getLatitud());
		lugar.setLongitud(lugLugar.getLongitud());
		lugar.setCodigoPostal(lugLugar.getCodigoPostal());
		lugar.setPrioridad(lugLugar.getPrioridad());
		lugar.setFechaCreacion(lugLugar.getFechaCreacion());
		lugar.setFechaModificacion(lugLugar.getFechaModificacion());
		lugar.setUsuUsuario(lugLugar.getUsuUsuario());
		lugar.setLugTipoLugar(lugLugar.getLugTipoLugar());
		lugar.setInfDivisionPolitica(lugLugar.getInfDivisionPolitica());
	}

	public void actionListenerMostrarCantonesPorProvincia() {
		listaCantones = managerDpa.findInfDivisonPoliticaByIdNivelAndIdDivisionPolitica(2, idProvincia);
	}

	public void actionListenerMostrarParroquiasPorCantones() {
		listaParroquias = managerDpa.findInfDivisonPoliticaByIdNivelAndIdDivisionPolitica(3, idCanton);
	}

	public String actionIrModificar(LugLugar lugar) {
		System.out.println("id_division_politica / parroquia: " + lugar.getInfDivisionPolitica()
				.getInfDivisionPolitica().getInfDivisionPolitica().getIdDivisionPolitica());
		System.out.println("id_division_politica_fk / canton: "
				+ lugar.getInfDivisionPolitica().getInfDivisionPolitica().getIdDivisionPolitica());
		System.out.println("id_nivel / provincia: " + lugar.getInfDivisionPolitica().getInfNivel().getIdNivel());
		// cargando datos
		this.lugar = lugar;
		idTipoLugar = lugar.getLugTipoLugar().getIdTipoLugar();
		idUsuario = lugar.getUsuUsuario().getIdUsuario();
		listaProvincias = managerDpa.findAllByNIvel(1);
		idProvincia = lugar.getInfDivisionPolitica().getInfDivisionPolitica().getInfDivisionPolitica()
				.getIdDivisionPolitica();
		actionListenerMostrarCantonesPorProvincia();
		idCanton = lugar.getInfDivisionPolitica().getInfDivisionPolitica().getIdDivisionPolitica();
		actionListenerMostrarParroquiasPorCantones();
		idParroquia = lugar.getInfDivisionPolitica().getIdDivisionPolitica();
		return "lugarmodificar";
	}

	public String actionIrCrear() {
		return "lugaragregar";
	}

	public List<LugLugar> getListaLugar() {
		return listaLugar;
	}

	public void setListaLugar(List<LugLugar> listaLugar) {
		this.listaLugar = listaLugar;
	}

	public LugLugar getLugar() {
		return lugar;
	}

	public void setLugar(LugLugar lugar) {
		this.lugar = lugar;
	}

	public int getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

	public int getIdTipoLugar() {
		return idTipoLugar;
	}

	public void setIdTipoLugar(int idTipoLugar) {
		this.idTipoLugar = idTipoLugar;
	}

	public List<InfDivisionPolitica> getListaProvincias() {
		return listaProvincias;
	}

	public void setListaProvincias(List<InfDivisionPolitica> listaProvincias) {
		this.listaProvincias = listaProvincias;
	}

	public int getIdProvincia() {
		return idProvincia;
	}

	public void setIdProvincia(int idProvincia) {
		this.idProvincia = idProvincia;
	}

	public int getIdCanton() {
		return idCanton;
	}

	public void setIdCanton(int idCanton) {
		this.idCanton = idCanton;
	}

	public int getIdParroquia() {
		return idParroquia;
	}

	public void setIdParroquia(int idParroquia) {
		this.idParroquia = idParroquia;
	}

	public List<InfDivisionPolitica> getListaCantones() {
		return listaCantones;
	}

	public void setListaCantones(List<InfDivisionPolitica> listaCantones) {
		this.listaCantones = listaCantones;
	}

	public List<InfDivisionPolitica> getListaParroquias() {
		return listaParroquias;
	}

	public void setListaParroquias(List<InfDivisionPolitica> listaParroquias) {
		this.listaParroquias = listaParroquias;
	}

	public MapModel getModelMapa() {
		return modelMapa;
	}

	public void setModelMapa(MapModel model) {
		this.modelMapa = model;
	}

}
