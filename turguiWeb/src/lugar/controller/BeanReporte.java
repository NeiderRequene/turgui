package lugar.controller;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import lugar.model.manager.ManagerEstadoReporte;
import lugar.model.manager.ManagerLugar;
import lugar.model.manager.ManagerReporte;
import multimedia.controller.JSFUtil;
import turgui.model.entities.LugEstadoReporte;
import turgui.model.entities.LugLugar;
import turgui.model.entities.LugReporte;
import turgui.model.entities.UsuUsuario;
import usuario.model.manager.ManagerUsuario;

import java.io.Serializable;
import java.util.List;

@Named
@SessionScoped
public class BeanReporte implements Serializable {
	private static final long serialVersionUID = 1L;
	private List<LugReporte> listaReportes;
	private List<LugEstadoReporte> listaEstadoReporte;
	private List<UsuUsuario> listaUsuarios;
	private List<LugLugar> listaLugar;
	private LugReporte reporte;
	private int idEstadoReporte;
	private int idUsuario;
	private int idLugar;
	@EJB
	private ManagerReporte managerReporte;
	@EJB
	private ManagerEstadoReporte managerEstadoReporte;
	@EJB
	private ManagerUsuario managerUsuario;
	@EJB
	private ManagerLugar managerLugar;

	@PostConstruct
	public void inicializar() {
		listaReportes = managerReporte.consultarReportes();
		listaEstadoReporte = managerEstadoReporte.consultarEstadoReporte();
		listaUsuarios = managerUsuario.findAllUsuarios();
		listaLugar = managerLugar.consultarLugar();
		reporte = new LugReporte();
	}

	public void actionIngresarReporte() {
		try {
			managerReporte.ingresarReporte(reporte.getObservacion(),
					managerEstadoReporte.buscarEstadoReportePorId(idEstadoReporte),
					managerUsuario.findUsuarioById(idUsuario), managerLugar.buscarLugarPorId(idLugar));
			listaReportes = managerReporte.consultarReportes();
			reporte = new LugReporte();
			JSFUtil.crearMensajeInfo("Reporte creado existosamente");
		} catch (Exception e) {
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListenerActualizarReporte() {
		try {
			managerReporte.actualizarReporte(reporte.getIdReporte(), reporte.getObservacion(),
					managerEstadoReporte.buscarEstadoReportePorId(idEstadoReporte),
					managerUsuario.findUsuarioById(idUsuario), managerLugar.buscarLugarPorId(idLugar));
			listaReportes = managerReporte.consultarReportes();
			JSFUtil.crearMensajeInfo("El reporte ha sido actualizado correctamente.");
			reporte = new LugReporte();
		} catch (Exception e) {
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListenerEliminarReporte() {
		try {
			managerReporte.eliminarReporte(reporte.getIdReporte());
			listaReportes = managerReporte.consultarReportes();
			JSFUtil.crearMensajeInfo("Reporte eliminado correctamente");
			reporte = new LugReporte();
		} catch (Exception e) {
			JSFUtil.crearMensajeError("Hubo problemas eliminando el contacto");
			e.printStackTrace();
		}
	}

	public void actionListenerCambiarReporte(LugReporte reporte) {
		this.reporte.setIdReporte(reporte.getIdReporte());
		this.reporte.setObservacion(reporte.getObservacion());
		this.reporte.setFechaCreacion(reporte.getFechaCreacion());
		this.reporte.setFechaModificacion(reporte.getFechaModificacion());
		this.reporte.setLugEstadoReporte(reporte.getLugEstadoReporte());
		this.reporte.setUsuUsuario(reporte.getUsuUsuario());
		this.reporte.setLugLugar(reporte.getLugLugar());
	}

	public List<LugReporte> getListaReportes() {
		return listaReportes;
	}

	public void setListaReportes(List<LugReporte> listaReportes) {
		this.listaReportes = listaReportes;
	}

	public List<LugEstadoReporte> getListaEstadoReporte() {
		return listaEstadoReporte;
	}

	public void setListaEstadoReporte(List<LugEstadoReporte> listaEstadoReporte) {
		this.listaEstadoReporte = listaEstadoReporte;
	}

	public List<UsuUsuario> getListaUsuarios() {
		return listaUsuarios;
	}

	public void setListaUsuarios(List<UsuUsuario> listaUsuarios) {
		this.listaUsuarios = listaUsuarios;
	}

	public List<LugLugar> getListaLugar() {
		return listaLugar;
	}

	public void setListaLugar(List<LugLugar> listaLugar) {
		this.listaLugar = listaLugar;
	}

	public LugReporte getReporte() {
		return reporte;
	}

	public void setReporte(LugReporte reporte) {
		this.reporte = reporte;
	}

	public int getIdEstadoReporte() {
		return idEstadoReporte;
	}

	public void setIdEstadoReporte(int idEstadoReporte) {
		this.idEstadoReporte = idEstadoReporte;
	}

	public int getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

	public int getIdLugar() {
		return idLugar;
	}

	public void setIdLugar(int idLugar) {
		this.idLugar = idLugar;
	}

}
