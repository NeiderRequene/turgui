package lugar.controller;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import lugar.model.manager.ManagerTipoLugar;
import multimedia.controller.JSFUtil;
import turgui.model.entities.LugTipoLugar;

import java.io.Serializable;
import java.util.List;

@Named
@SessionScoped
public class BeanTipoLugar implements Serializable {
	private static final long serialVersionUID = 1L;
	private List<LugTipoLugar> listaTipoLugar;
	private LugTipoLugar tipoLugar;
	@EJB
	private ManagerTipoLugar managerTipoLugar;

	@PostConstruct
	public void inicializar() {
		listaTipoLugar = managerTipoLugar.consultarTipoLugar();
		tipoLugar = new LugTipoLugar();
	}

	public void actionListenerIngresarTipoLugar() {
		try {
			managerTipoLugar.ingresarTipoLugar(tipoLugar);
			listaTipoLugar = managerTipoLugar.consultarTipoLugar();
			tipoLugar = new LugTipoLugar();
			JSFUtil.crearMensajeInfo("Tipo de Lugar creado existosamente");
		} catch (Exception e) {
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListenerActualizarTipoLugar() {
		try {
			managerTipoLugar.actualizarTipoLugar(tipoLugar);
			listaTipoLugar = managerTipoLugar.consultarTipoLugar();
			JSFUtil.crearMensajeInfo(
					"El tipo de lugar " + tipoLugar.getNombre() + " ha sido actualizado correctamente.");
			tipoLugar = new LugTipoLugar();
		} catch (Exception e) {
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListenerEliminarTipoLugar() {
		try {
			managerTipoLugar.eliminarTipoLugar(tipoLugar.getIdTipoLugar());
			listaTipoLugar = managerTipoLugar.consultarTipoLugar();
			tipoLugar = new LugTipoLugar();
			JSFUtil.crearMensajeInfo("Tipo de lugar eliminado correctamente");
		} catch (Exception e) {
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListenerCambiarTipoLugar(LugTipoLugar lugar) {
		tipoLugar.setIdTipoLugar(lugar.getIdTipoLugar());
		tipoLugar.setNombre(lugar.getNombre());
		tipoLugar.setDescripcion(lugar.getDescripcion());
	}

	public List<LugTipoLugar> getListaTipoLugar() {
		return listaTipoLugar;
	}

	public void setListaTipoLugar(List<LugTipoLugar> listaTipoLugar) {
		this.listaTipoLugar = listaTipoLugar;
	}

	public LugTipoLugar getTipoLugar() {
		return tipoLugar;
	}

	public void setTipoLugar(LugTipoLugar tipoLugar) {
		this.tipoLugar = tipoLugar;
	}
}
