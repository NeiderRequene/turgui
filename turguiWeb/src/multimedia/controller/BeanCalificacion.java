package multimedia.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import lugar.model.manager.ManagerLugar;
import multimedia.model.manager.ManagerCalificacion;
import turgui.model.entities.LugLugar;
import turgui.model.entities.MulCalificacion;
import turgui.model.entities.UsuUsuario;
import usuario.model.manager.ManagerUsuario;

@Named
@SessionScoped
public class BeanCalificacion implements Serializable {
	private static final long serialVersionUID = 1L;

	private List<MulCalificacion> listaCalificaciones;
	private List<LugLugar> listaLugares;
	private List<UsuUsuario> listaUsuarios;
	private MulCalificacion calificacion;
	private Integer idLugar;
	private Integer idUsuario;

	@EJB
	private ManagerCalificacion managerCalificacion;
	@EJB
	private ManagerLugar managerLugar;
	@EJB
	private ManagerUsuario managerUsuario;

	@PostConstruct
	public void inicializar() {
		listaCalificaciones = managerCalificacion.findAllCalificacion();
		listaLugares = managerLugar.consultarLugar();
		listaUsuarios = managerUsuario.findAllUsuarios();
		calificacion = new MulCalificacion();

	}

	public void actionListenerInsertarCalificacion() {
		try {
			managerCalificacion.insertCalificacion(calificacion, idLugar, idUsuario);
			listaCalificaciones = managerCalificacion.findAllCalificacion();
			listaLugares = managerLugar.consultarLugar();
			listaUsuarios = managerUsuario.findAllUsuarios();
			calificacion = new MulCalificacion();
			JSFUtil.crearMensajeInfo("Calificación agregados correctamente");

		} catch (Exception e) {
			// TODO: handle exception
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}

	public void cambiarDatosCalificacion(MulCalificacion nueva_calificacion) {
		if (nueva_calificacion != null) {
			idLugar = nueva_calificacion.getLugLugar().getIdLugar();
			idUsuario = nueva_calificacion.getUsuUsuario().getIdUsuario();
			calificacion.setIdCalificacion(nueva_calificacion.getIdCalificacion());
			calificacion.setUsuUsuario(nueva_calificacion.getUsuUsuario());
			calificacion.setLugLugar(nueva_calificacion.getLugLugar());
			calificacion.setFechaCreacion(nueva_calificacion.getFechaCreacion());
			calificacion.setValor(nueva_calificacion.getValor());
			calificacion.setObservacion(nueva_calificacion.getObservacion());
		}

	}

	public void actionListenerUpdateCalificacion() {
		try {
			managerCalificacion.updateCalificacion(calificacion, idLugar, idUsuario);
			listaCalificaciones = managerCalificacion.findAllCalificacion();
			listaLugares = managerLugar.consultarLugar();
			listaUsuarios = managerUsuario.findAllUsuarios();
			calificacion = new MulCalificacion();
			JSFUtil.crearMensajeInfo("Calificación actualizada correctamente");

		} catch (Exception e) {
			// TODO: handle exception
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListenerEliminarCalificacion() {
		try {
			managerCalificacion.deleteCalificacion(calificacion.getIdCalificacion());
			listaCalificaciones = managerCalificacion.findAllCalificacion();
			listaLugares = managerLugar.consultarLugar();
			listaUsuarios = managerUsuario.findAllUsuarios();
			calificacion = new MulCalificacion();
			JSFUtil.crearMensajeInfo("Calificación eliminada correctamente");

		} catch (Exception e) {
			// TODO: handle exception
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}

	public List<MulCalificacion> getListaCalificaciones() {
		return listaCalificaciones;
	}

	public void setListaCalificaciones(List<MulCalificacion> listaCalificaciones) {
		this.listaCalificaciones = listaCalificaciones;
	}

	public List<LugLugar> getListaLugares() {
		return listaLugares;
	}

	public void setListaLugares(List<LugLugar> listaLugares) {
		this.listaLugares = listaLugares;
	}

	public MulCalificacion getCalificacion() {
		return calificacion;
	}

	public void setCalificacion(MulCalificacion calificacion) {
		this.calificacion = calificacion;
	}

	public Integer getIdLugar() {
		return idLugar;
	}

	public void setIdLugar(Integer idLugar) {
		this.idLugar = idLugar;
	}

	public List<UsuUsuario> getListaUsuarios() {
		return listaUsuarios;
	}

	public void setListaUsuarios(List<UsuUsuario> listaUsuarios) {
		this.listaUsuarios = listaUsuarios;
	}

	public Integer getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

}
