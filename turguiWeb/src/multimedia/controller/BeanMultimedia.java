package multimedia.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import lugar.model.manager.ManagerLugar;
import lugar.model.manager.ManagerTipoLugar;
import multimedia.model.manager.ManagerMultimedia;
import multimedia.model.manager.ManagerTipoArchivo;
import turgui.model.entities.LugLugar;
import turgui.model.entities.MulMultimedia;
import turgui.model.entities.MulTipoArchivo;
import usuario.model.manager.ManagerUsuario;

@Named
@SessionScoped
public class BeanMultimedia implements Serializable {

	private static final long serialVersionUID = 1L;

	private MulMultimedia multimedia;
	private Integer idLugarMultimedia;
	private Integer idTipoArchivoMultimedia;
	private boolean numPortada;
	private List<MulMultimedia> listaMultimedia;
	private List<LugLugar> listaLugares;
	private List<MulTipoArchivo> listaTipoArchivo;
	private UploadedFile archivoSeleccionado;
	private String extensionesTipoArchivo;

	int numeroLugares;
	int numeroUsuario;
	int numeroTipoLugares;
	int numeroTipoArchivo;

	@EJB
	private ManagerMultimedia managerMultimedia;
	@EJB
	private ManagerTipoArchivo managerTipoArchivo;
	@EJB
	private ManagerUsuario managerUsuario;
	@EJB
	private ManagerTipoLugar managerTipoLugar;
	@EJB
	private ManagerLugar managerLugar;

	@PostConstruct
	public void inicializar() {
		listaMultimedia = managerMultimedia.findAllMultimedia();
		listaLugares = managerMultimedia.findAllLugar();
		listaTipoArchivo = managerTipoArchivo.findAllTipoArchivo();
		extensionesTipoArchivo = "";
		idTipoArchivoMultimedia = listaTipoArchivo.get(0).getIdTipoArchivo();
		multimedia = new MulMultimedia();
		numeroUsuario = managerUsuario.findNumeroUsuarios();
		numeroLugares = managerLugar.findNumeroLugares();
		numeroTipoLugares = managerTipoLugar.findNumeroTipoLugares();
		numeroTipoArchivo = managerTipoArchivo.findNumeroTipoArchivo();
	}

	public void actionListenerInsertarMultimedia() {
		try {
			String contentType = archivoSeleccionado.getContentType();
			byte[] contents = archivoSeleccionado.getContents();
			managerMultimedia.insertMultimedia(multimedia, idLugarMultimedia, idTipoArchivoMultimedia, numPortada,
					contentType, contents);
			System.out.println(archivoSeleccionado.getFileName());
			listaMultimedia = managerMultimedia.findAllMultimedia();
			multimedia = new MulMultimedia();
			JSFUtil.crearMensajeInfo("Datos agregados correctamente");

		} catch (Exception e) {
			// TODO: handle exception
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListenerEliminarMultimedia() {
		try {
			managerMultimedia.deleteMultimedia(multimedia.getIdMultimedia());
			listaMultimedia = managerMultimedia.findAllMultimedia();
			multimedia = new MulMultimedia();
			JSFUtil.crearMensajeInfo("Datos eliminado correctamente");

		} catch (Exception e) {
			// TODO: handle exception
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}

	public void cambiarDatosMultimedia(MulMultimedia nuevo_multimedia) {
		if (nuevo_multimedia != null) {
			multimedia.setDescripcion(nuevo_multimedia.getDescripcion());
			multimedia.setDescripcionCorta(nuevo_multimedia.getDescripcionCorta());
			multimedia.setDireccion(nuevo_multimedia.getDireccion());
			multimedia.setFechaCreacion(nuevo_multimedia.getFechaCreacion());
			multimedia.setFechaModificacion(nuevo_multimedia.getFechaModificacion());
			multimedia.setIdMultimedia(nuevo_multimedia.getIdMultimedia());
			multimedia.setLugLugar(nuevo_multimedia.getLugLugar());
			multimedia.setMulTipoArchivo(nuevo_multimedia.getMulTipoArchivo());
			multimedia.setNombre(nuevo_multimedia.getNombre());
			multimedia.setPortada(nuevo_multimedia.getPortada());
		}

	}

	public void obtenerExtensionTipoArchivo() {
		MulTipoArchivo nuevo_tipoArchivo = managerTipoArchivo.findTipoArchivoById(idTipoArchivoMultimedia);
		String extension[] = nuevo_tipoArchivo.getExtension().split(",");
		extensionesTipoArchivo = "";
		for (int i = 0; i < extension.length; i++) {
			if (i == extension.length - 1) {
				extensionesTipoArchivo = extensionesTipoArchivo + extension[i];
			} else {
				extensionesTipoArchivo = extensionesTipoArchivo + extension[i] + "|";
			}

		}
		System.out.println("Lista de extensiones: " + extensionesTipoArchivo);
	}

	public void upload() {

		String fileName = archivoSeleccionado.getFileName();
		String contentType = archivoSeleccionado.getContentType();
		byte[] contents = archivoSeleccionado.getContents(); // Or getInputStream()
		System.out.println("Aliii" + fileName);
		// ... Save it, now!
	}

	public String actionIrCrear() {
		return "agregarMultimedia";
	}

	public MulMultimedia getMultimedia() {
		return multimedia;
	}

	public void setMultimedia(MulMultimedia multimedia) {
		this.multimedia = multimedia;
	}

	public List<MulMultimedia> getListaMultimedia() {
		return listaMultimedia;
	}

	public void setListaMultimedia(List<MulMultimedia> listaMultimedia) {
		this.listaMultimedia = listaMultimedia;
	}

	public List<LugLugar> getListaLugares() {
		return listaLugares;
	}

	public void setListaLugares(List<LugLugar> listaLugares) {
		this.listaLugares = listaLugares;
	}

	public Integer getIdLugarMultimedia() {
		return idLugarMultimedia;
	}

	public void setIdLugarMultimedia(Integer idLugarMultimedia) {
		this.idLugarMultimedia = idLugarMultimedia;
	}

	public Integer getIdTipoArchivoMultimedia() {
		return idTipoArchivoMultimedia;
	}

	public void setIdTipoArchivoMultimedia(Integer idTipoArchivoMultimedia) {
		this.idTipoArchivoMultimedia = idTipoArchivoMultimedia;
	}

	public List<MulTipoArchivo> getListaTipoArchivo() {
		return listaTipoArchivo;
	}

	public void setListaTipoArchivo(List<MulTipoArchivo> listaTipoArchivo) {
		this.listaTipoArchivo = listaTipoArchivo;
	}

	public boolean isNumPortada() {
		return numPortada;
	}

	public void setNumPortada(boolean numPortada) {
		this.numPortada = numPortada;
	}

	public UploadedFile getArchivoSeleccionado() {
		return archivoSeleccionado;
	}

	public void setArchivoSeleccionado(UploadedFile archivoSeleccionado) {
		this.archivoSeleccionado = archivoSeleccionado;
	}

	public String getExtensionesTipoArchivo() {
		return extensionesTipoArchivo;
	}

	public void setExtensionesTipoArchivo(String extensionesTipoArchivo) {
		this.extensionesTipoArchivo = extensionesTipoArchivo;
	}

	public int getNumeroLugares() {
		return numeroLugares;
	}

	public void setNumeroLugares(int numeroLugares) {
		this.numeroLugares = numeroLugares;
	}

	public int getNumeroUsuario() {
		return numeroUsuario;
	}

	public void setNumeroUsuario(int numeroUsuario) {
		this.numeroUsuario = numeroUsuario;
	}

	public int getNumeroTipoLugares() {
		return numeroTipoLugares;
	}

	public void setNumeroTipoLugares(int numeroTipoLugares) {
		this.numeroTipoLugares = numeroTipoLugares;
	}

	public int getNumeroTipoArchivo() {
		return numeroTipoArchivo;
	}

	public void setNumeroTipoArchivo(int numeroTipoArchivo) {
		this.numeroTipoArchivo = numeroTipoArchivo;
	}

}
