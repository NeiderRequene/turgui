package multimedia.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import multimedia.model.manager.ManagerTipoArchivo;
import turgui.model.entities.MulTipoArchivo;



@Named
@SessionScoped
public class BeanTipoArchivo implements Serializable{
	private static final long serialVersionUID = 1L;
	private MulTipoArchivo tipoArchivo;
	private List<MulTipoArchivo> listaTipoArchivo;
	
	@EJB
	private ManagerTipoArchivo managerTipoArchivo;
	
	@PostConstruct
	public void inicializar() {
		listaTipoArchivo = managerTipoArchivo.findAllTipoArchivo();
		 tipoArchivo= new MulTipoArchivo();
	}
	public void actionListenerInsertarTipoArchivo() {
		try {
			managerTipoArchivo.insertTipoArchivo(tipoArchivo);
			listaTipoArchivo = managerTipoArchivo.findAllTipoArchivo();
			tipoArchivo = new MulTipoArchivo();
			JSFUtil.crearMensajeInfo("Tipo de Archivo agregado correctamente");

		} catch (Exception e) {
			// TODO: handle exception
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void actionListenerEliminarTipoArchivo() {
		try {
			managerTipoArchivo.deleteTipoArchivo(tipoArchivo.getIdTipoArchivo());
			listaTipoArchivo = managerTipoArchivo.findAllTipoArchivo();
			tipoArchivo = new MulTipoArchivo();
			JSFUtil.crearMensajeInfo("Tipo de archivo eliminado correctamente");

		} catch (Exception e) {
			// TODO: handle exception
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void actionListenerActualizarTipoArchivo() {
		try {
			managerTipoArchivo.updateTipoArchivo(tipoArchivo);
			listaTipoArchivo = managerTipoArchivo.findAllTipoArchivo();
			tipoArchivo = new MulTipoArchivo();
			JSFUtil.crearMensajeInfo("Tipo de archivo actualizado correctamente");

		} catch (Exception e) {
			// TODO: handle exception
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void cambiarDatosTipoArchivo(MulTipoArchivo nuevo_tipoArchivo) {
		
		tipoArchivo.setIdTipoArchivo(nuevo_tipoArchivo.getIdTipoArchivo());
		tipoArchivo.setNombre(nuevo_tipoArchivo.getNombre());
		tipoArchivo.setExtension(nuevo_tipoArchivo.getExtension());
		System.out.println("Cambiando: "+tipoArchivo.getNombre());
		
	}

	

	public MulTipoArchivo getTipoArchivo() {
		return tipoArchivo;
	}
	public void setTipoArchivo(MulTipoArchivo tipoArchivo) {
		this.tipoArchivo = tipoArchivo;
	}
	public List<MulTipoArchivo> getListaTipoArchivo() {
		return listaTipoArchivo;
	}

	public void setListaTipoArchivo(List<MulTipoArchivo> listaTipoArchivo) {
		this.listaTipoArchivo = listaTipoArchivo;
	}
	
	
	

}
