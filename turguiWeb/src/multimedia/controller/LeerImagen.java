package multimedia.controller;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import multimedia.model.manager.ManagerMultimedia;
import turgui.model.entities.MulMultimedia;

/**
 * Servlet implementation class LeerImagen
 */
@WebServlet("/LeerImagen")
public class LeerImagen extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@EJB
	private ManagerMultimedia managerMultimedia;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LeerImagen() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int codigoLugar = Integer.parseInt(request.getParameter("codigo"));
		System.out.println("codigoLugar: "+codigoLugar);
		List<MulMultimedia> lista_multimedia = managerMultimedia.findMultimediaByIdLugar(codigoLugar);
		if(lista_multimedia.size()<=0) {
			return;
		}
		MulMultimedia multimedia = lista_multimedia.get(0);
		System.out.println("Nombre multimedia: "+multimedia.getNombre());
		System.out.println("Ruta: "+multimedia.getDireccion());
		// TODO Auto-generated method stub
		// CODIGO PARA ESCRIBIR UNA IMAGEN DESDE UN FICHERO VIA SERVLET
		response.setContentType("image/png");
		ServletOutputStream out = response.getOutputStream();
		BufferedInputStream in = null;

		try {
			System.out.println("Aquiii");
			in = new BufferedInputStream(new FileInputStream(multimedia.getDireccion()));
			System.out.println("Archivo leido");

			int count = 0;
			byte b[] = new byte[8192]; // El tamaño se pone a ojo, por ahí recomiendan este, no sé por qué.
			while ((count = in.read(b)) != -1) {
				out.write(b, 0, count);
			}
			in.close();
			out.close();
		} finally {
			if (in != null)
				in.close(); // bastante importante!
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
