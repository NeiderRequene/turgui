package template.controller;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import turgui.model.entities.LugEstadoReporte;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Named
@SessionScoped
public class ControllerTemplate implements Serializable {
	private static final long serialVersionUID = 1L;
	private List<LugEstadoReporte> listaEstadoReporte;
	private LugEstadoReporte estadoReporte;
	
	@PostConstruct
	public void inicializar() {
		 listaEstadoReporte = new ArrayList<LugEstadoReporte>();
		 LugEstadoReporte estadoReporte = new LugEstadoReporte();
		 estadoReporte.setIdEstadoReporte(1);
		 estadoReporte.setNombre("Activado");;
		 listaEstadoReporte.add(estadoReporte);
		 //
		 estadoReporte = new LugEstadoReporte();
		 estadoReporte.setIdEstadoReporte(1);
		 estadoReporte.setNombre("Activado");;
	}

	public List<LugEstadoReporte> getListaEstadoReporte() {
		return listaEstadoReporte;
	}

	public void setListaEstadoReporte(List<LugEstadoReporte> listaEstadoReporte) {
		this.listaEstadoReporte = listaEstadoReporte;
	}

	public LugEstadoReporte getEstadoReporte() {
		return estadoReporte;
	}

	public void setEstadoReporte(LugEstadoReporte estadoReporte) {
		this.estadoReporte = estadoReporte;
	}

}
