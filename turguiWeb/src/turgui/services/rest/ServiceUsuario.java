package turgui.services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import turgui.model.dto.UsuarioDTO;
import turgui.model.entities.UsuUsuario;
import usuario.model.manager.ManagerUsuario;

@RequestScoped
@Path("/usuario")
@Produces("application/json")
@Consumes("application/json")
public class ServiceUsuario {
	
	@EJB
	private ManagerUsuario mUsuario;
	
	
	@GET
	public List<UsuarioDTO> findAllEstudiantes(){
		List<UsuarioDTO> listaUsuarios = new ArrayList();
		
		for (UsuUsuario u : mUsuario.findAllUsuarios()) { 
			UsuarioDTO usuario = new UsuarioDTO();
			
			usuario.setIdUsuario(u.getIdUsuario());
			usuario.setNombres(u.getNombres());
			usuario.setApellidos(u.getApellidos());
			usuario.setCorreo(u.getCorreo());
			usuario.setCuidad(u.getCuidad());
			usuario.setEstadoCivil(u.getEstadoCivil());
			usuario.setProfesion(u.getProfesion());
			usuario.setSexo(u.getSexo());
			
			listaUsuarios.add(usuario);
			
		}
		
		return listaUsuarios;
	}

}
