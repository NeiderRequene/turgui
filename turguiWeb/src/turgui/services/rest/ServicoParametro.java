package turgui.services.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import informacion.model.manager.ManagerInformacion;
import turgui.model.entities.InfParametro;

@RequestScoped
@Path("/infParametro")
@Produces("application/json")
@Consumes("application/json")
public class ServicoParametro {

	
	@EJB
	private ManagerInformacion manaInfo;

	@GET
	public List<InfParametro> listarDatos() {

		return manaInfo.findAllInfParametros();
	}


	@POST
	public String ingresarParametro(InfParametro parametro) throws Exception {
		String resultado = manaInfo.insertParametro(parametro);
		
		return "{\"resultado\":\"" + resultado + "\"}";
	}


	@DELETE
	@Path("/{idParametro}")
	public String EliminarParametroPorId(@PathParam("idParametro") int idParametro) {
		String resultado = manaInfo.deleteParametro(idParametro);

		return "{\"resultado\":\"" + resultado + "\"}";
	}
	
	@PUT
	public String actualizarParametro(InfParametro parametro) throws Exception {
		String resultado = manaInfo.updateInfParametro(parametro);
		
		return "{\"resultado\":\"" + resultado + "\"}";
	}

	
}
