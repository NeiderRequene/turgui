package usuario.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import turgui.model.entities.UsuBitacora;
import usuario.model.manager.ManagerAuditoria;

@Named
@SessionScoped
public class BeanAuditoria implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@EJB
	private ManagerAuditoria managerAuditoria;
	
	private List<UsuBitacora> listaBitacora;

	public BeanAuditoria() {
		// TODO Auto-generated constructor stub
	}
	
	@PostConstruct
	public void inicializar() {
		listaBitacora = managerAuditoria.findAllBitacoras();
	}
	
	public void actualizarBitacora() {
		listaBitacora = managerAuditoria.findAllBitacoras();
	}

	public List<UsuBitacora> getListaBitacora() {
		return listaBitacora;
	}

	public void setListaBitacora(List<UsuBitacora> listaBitacora) {
		this.listaBitacora = listaBitacora;
	}
	
	
	

}
