package usuario.controller;

import java.io.Serializable;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import multimedia.controller.JSFUtil;
import turgui.model.dto.LoginDTO;
import usuario.model.manager.ManagerAuditoria;
import usuario.model.manager.ManagerSeguridad;

@Named
@SessionScoped
public class BeanLogin implements Serializable {

	private static final long serialVersionUID = 1L;

	private String correoUsuario;
	private String contrasenia;
	private String tipoUsuario;
	private String rol;
	private boolean acceso;

	private String respuesta;
	private String pregunta;
	private String nuevaContrasena;

	@EJB
	private ManagerSeguridad managerSeguridad;

	@EJB
	private ManagerAuditoria managerAuditoria;

	private LoginDTO loginDTO;

	public BeanLogin() {
		// TODO Auto-generated constructor stub
	}

	@PostConstruct
	public void inicializar() {

	}

	public String accederTurgui() {
		acceso = false;
		try {
			loginDTO = new LoginDTO();
			loginDTO = managerSeguridad.iniciarSesion(correoUsuario, contrasenia, rol);
			tipoUsuario = loginDTO.getRol();

			System.out.println(tipoUsuario);
			System.out.println(loginDTO.getRutaAcceso());
			System.out.println(tipoUsuario);
			// Aqui lo de auditoria
			managerAuditoria.crearEventoBitacora(correoUsuario, this.getClass(), "Inicio sesion", "Inicio de sesion",
					"local_host");
			return loginDTO.getRutaAcceso() + "?faces-redirect=true";
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			JSFUtil.crearMensajeError(e.getMessage());
		}
		return "";
	}

	public String actionCerrarSesion() {

		try {
			// Aqui lo de auditoria
			managerAuditoria.crearEventoBitacora(loginDTO.getCorreo(), this.getClass(), "Cerrar sesion",
					"Inicio de sesion", "local_host");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			JSFUtil.crearMensajeError(e.getMessage());
		}
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		loginDTO = null;
		System.out.println("cerrando sesiòn");
		return "/faces/visitante/index.xhtml?faces-redirect=true";
	}

	public String Responderpregunta() {

		pregunta = managerSeguridad.consultarPregunta(correoUsuario);
		System.out.println("usuario: " + correoUsuario);
		
		if (pregunta.equals("")) {
			return "/superadministrador/usuUsuario/recuperarContrasena.xhtml?faces-redirect=true";
		} else {
			return "/superadministrador/usuUsuario/responderPregunta.xhtml?faces-redirect=true";
		}
	}

	public String ResponderVerificarPregunta() throws Exception {
		if (managerSeguridad.verificarPreguntaSeguridad(correoUsuario, pregunta, respuesta) == true) {
			return "/superadministrador/usuUsuario/nuevaContrasena.xhtml?faces-redirect=true";
		} else {
			return "/superadministrador/usuUsuario/responderPregunta.xhtml?faces-redirect=true";
		}
	}

	public String actionCambiarContrasena() {
		managerSeguridad.cambiarContrasena(correoUsuario, contrasenia);
		try {
			managerAuditoria.crearEventoBitacora(correoUsuario, this.getClass(), "Cambiar password",
					"Inicio de sesion", "local_host");
		} catch (Exception e) {
			// TODO: handle exception
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
		return "/login.xhtml?faces-redirect=true";
	}

	public void verificarSesion() {
		ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
		String requestPath = ec.getRequestPathInfo();
		try {
			if (loginDTO == null) {
				ec.redirect(ec.getRequestContextPath() + "/faces/visitante/index.xhtml");
			} else {
				
				ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
				Map<String, Object> sessionMap = externalContext.getSessionMap();
				sessionMap.put("loginDTO", loginDTO);
				
				// Ahora valido las rutas
				if (requestPath.contains("/usuario") && loginDTO.getRutaAcceso().startsWith("/faces/usuario"))
					return;
				if (requestPath.contains("/administrador")
						&& loginDTO.getRutaAcceso().startsWith("/faces/administrador"))
					return;
				if (requestPath.contains("/superadministrador")
						&& loginDTO.getRutaAcceso().startsWith("/faces/superadministrador"))
					return;
				// caso contrario significa que hizo login pero intenta acceder a ruta no
				// permitida:
				ec.redirect(ec.getRequestContextPath() + "/faces/vistante/index.xhtml");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public String getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}

	public String getPregunta() {
		return pregunta;
	}

	public void setPregunta(String pregunta) {
		this.pregunta = pregunta;
	}

	public String getNuevaContrasena() {
		return nuevaContrasena;
	}

	public void setNuevaContrasena(String nuevaContrasena) {
		this.nuevaContrasena = nuevaContrasena;
	}

	public LoginDTO getLoginDTO() {
		return loginDTO;
	}

	public void setLoginDTO(LoginDTO loginDTO) {
		this.loginDTO = loginDTO;
	}

	public String getCorreoUsuario() {
		return correoUsuario;
	}

	public void setCorreoUsuario(String correoUsuario) {
		this.correoUsuario = correoUsuario;
	}

	public String getContrasenia() {
		return contrasenia;
	}

	public void setContrasenia(String contrasenia) {
		this.contrasenia = contrasenia;
	}

	public String getTipoUsuario() {
		return tipoUsuario;
	}

	public void setTipoUsuario(String tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

	public boolean isAcceso() {
		return acceso;
	}

	public void setAcceso(boolean acceso) {
		this.acceso = acceso;
	}

	public String getRol() {
		return rol;
	}

	public void setRol(String rol) {
		this.rol = rol;
	}

}
