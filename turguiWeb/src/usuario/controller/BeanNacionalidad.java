package usuario.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import multimedia.controller.JSFUtil;
import turgui.model.entities.UsuNacionalidad;
import turgui.model.entities.UsuUsuario;
import usuario.model.manager.ManagerNacionalidad;

@Named
@SessionScoped
public class BeanNacionalidad implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<UsuNacionalidad> listaNacionalidades;
	private UsuNacionalidad nacionalidad;

	@EJB
	ManagerNacionalidad managerNacionalidad;

	@PostConstruct
	public void inicializar() {

		listaNacionalidades = managerNacionalidad.findAllNacionalidad();
		nacionalidad = new UsuNacionalidad();

	}

	public void actionInsertarNacionalidad() {
		try {
			managerNacionalidad.insertarNacionalidad(nacionalidad);
			listaNacionalidades = managerNacionalidad.findAllNacionalidad();
			nacionalidad = new UsuNacionalidad();
			JSFUtil.crearMensajeInfo("Se ha registrado nacionalidad");
		} catch (Exception e) {
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionEliminarNacionalidad() {
		try {
			managerNacionalidad.eliminarNacionalidad(nacionalidad.getIdNacionalidad());
			listaNacionalidades = managerNacionalidad.findAllNacionalidad();
			nacionalidad = new UsuNacionalidad();

			JSFUtil.crearMensajeInfo("Se ha eliminado nacionalidad");
		} catch (Exception e) {
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void actionActualizarNacionalidad() {
		try {
			managerNacionalidad.actualizarNacionalidad(nacionalidad);
			listaNacionalidades = managerNacionalidad.findAllNacionalidad();
			nacionalidad = new UsuNacionalidad();

			
			JSFUtil.crearMensajeInfo("Se ha actualizado nacionalidad");
		} catch (Exception e) {
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}
	
	
	public void cambiarDatosNacionalidad(UsuNacionalidad nacionalidad_nuevo) {
		nacionalidad.setIdNacionalidad(nacionalidad_nuevo.getIdNacionalidad());
		nacionalidad.setNombre(nacionalidad_nuevo.getNombre());
		
	}

	public List<UsuNacionalidad> getListaNacionalidades() {
		return listaNacionalidades;
	}

	public void setListaNacionalidades(List<UsuNacionalidad> listaNacionalidades) {
		this.listaNacionalidades = listaNacionalidades;
	}

	public UsuNacionalidad getNacionalidad() {
		return nacionalidad;
	}

	public void setNacionalidad(UsuNacionalidad nacionalidad) {
		this.nacionalidad = nacionalidad;
	}


}
