package usuario.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import multimedia.controller.JSFUtil;
import turgui.model.entities.UsuRol;
import usuario.model.manager.ManagerRol;

@Named
@SessionScoped
public class BeanRol implements Serializable {
	private static final long serialVersionUID = 1L;

	@EJB
	private ManagerRol managerRol;

	private List<UsuRol> listaRoles;
	private UsuRol rol;

	public BeanRol() {
	}

	@PostConstruct
	public void inicializar() {
		listaRoles = managerRol.findAllRoles();
		rol = new UsuRol();
	}

	public void actionInsertarRol() {
		try {
			listaRoles = managerRol.findAllRoles();
			managerRol.insertarRol(rol);
			listaRoles = managerRol.findAllRoles();
			rol = new UsuRol();
			JSFUtil.crearMensajeInfo("Se insertado un nuevo rol");
		} catch (Exception e) {
			// TODO: handle exception
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionEliminarRol() {
		try {
			listaRoles = managerRol.findAllRoles();
			managerRol.eliminarRol(rol.getIdRol());
			listaRoles = managerRol.findAllRoles();
			rol = new UsuRol();
			JSFUtil.crearMensajeInfo("Se eliminado un nuevo rol");
		} catch (Exception e) {
			// TODO: handle exception
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionActualizarRol() {
		try {
			listaRoles = managerRol.findAllRoles();
			managerRol.actualizarRol(rol);
			listaRoles = managerRol.findAllRoles();
			rol = new UsuRol();
			JSFUtil.crearMensajeInfo("Se actualizado un nuevo rol");
		} catch (Exception e) {
			// TODO: handle exception
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}


	public void cambiarDatosRol(UsuRol rol_nuevo) {
		rol.setIdRol(rol_nuevo.getIdRol());
		rol.setNombre(rol_nuevo.getNombre());
	}

	public ManagerRol getManagerRol() {
		return managerRol;
	}

	public void setManagerRol(ManagerRol managerRol) {
		this.managerRol = managerRol;
	}

	public List<UsuRol> getListaRoles() {
		return listaRoles;
	}

	public void setListaRoles(List<UsuRol> listaRoles) {
		this.listaRoles = listaRoles;
	}

	public UsuRol getRol() {
		return rol;
	}

	public void setRol(UsuRol rol) {
		this.rol = rol;
	}

}
