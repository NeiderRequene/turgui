package usuario.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import multimedia.controller.JSFUtil;
import turgui.model.entities.UsuRol;
import turgui.model.entities.UsuUsuario;
import turgui.model.entities.UsuUsuarioRol;
import usuario.model.manager.ManagerRol;
import usuario.model.manager.ManagerUsuario;
import usuario.model.manager.ManagerUsuarioRol;

@Named
@SessionScoped
public class BeanUsuRol implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ManagerUsuarioRol managerUsuarioRol;

	@EJB
	private ManagerUsuario managerUsuario;

	@EJB
	private ManagerRol managerRol;

	private List<UsuRol> listaRoles;
	private List<UsuUsuario> listaUsuarios;
	private List<UsuUsuarioRol> listaUsuariosRoles;

	private UsuUsuarioRol usurol;
	private int idUsuario;
	private int idRol;

	public BeanUsuRol() {
	}

	@PostConstruct
	public void inicializar() {
		System.out.println("----------------------------- BEAN_USUROL -------------------------------------");
		listaRoles = managerRol.findAllRoles();
		listaUsuarios = managerUsuario.findAllUsuarios();
		listaUsuariosRoles = managerUsuarioRol.findAllUsuarioRoles();
		usurol = new UsuUsuarioRol();
		idUsuario = 0;
		idRol = 0;
	}

	public void actionInsertarUsuRol() {
		try {
			listaRoles = managerRol.findAllRoles();
			listaUsuarios = managerUsuario.findAllUsuarios();
			managerUsuarioRol.insertarUsuarioRol(idRol, idUsuario);
			listaUsuariosRoles = managerUsuarioRol.findAllUsuarioRoles();
			usurol = new UsuUsuarioRol();
			idUsuario = 0;
			idRol = 0;

			JSFUtil.crearMensajeInfo("Se ha insertado la asignacion");
		} catch (Exception e) {
			// TODO: handle exception
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionEliminarUsuRol() {
		try {
			listaRoles = managerRol.findAllRoles();
			listaUsuarios = managerUsuario.findAllUsuarios();
			managerUsuarioRol.eliminarUsuarioRol(usurol);
			listaUsuariosRoles = managerUsuarioRol.findAllUsuarioRoles();
			usurol = new UsuUsuarioRol();
			idUsuario = 0;
			idRol = 0;
			JSFUtil.crearMensajeInfo("Se ha eliminado la asignacion");
		} catch (Exception e) {
			// TODO: handle exception
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionActualizarUsuRol() {
		try {
			listaRoles = managerRol.findAllRoles();
			listaUsuarios = managerUsuario.findAllUsuarios();
			managerUsuarioRol.actualizarUsuarioRol(usurol, idRol, idUsuario);
			listaUsuariosRoles = managerUsuarioRol.findAllUsuarioRoles();
			usurol = new UsuUsuarioRol();
			idUsuario = 0;
			idRol = 0;
			JSFUtil.crearMensajeInfo("Se ha actualizado la asignacion");
		} catch (Exception e) {
			// TODO: handle exception
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}

	public void cambiarDatosUsuRol(UsuUsuarioRol usurol) {
		listaRoles = managerRol.findAllRoles();
		listaUsuarios = managerUsuario.findAllUsuarios();
		usurol.setUsuRol(usurol.getUsuRol());
		usurol.setUsuUsuario(usurol.getUsuUsuario());
	}

	public int getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

	public int getIdRol() {
		return idRol;
	}

	public void setIdRol(int idRol) {
		this.idRol = idRol;
	}

	public List<UsuRol> getListaRoles() {
		return listaRoles;
	}

	public void setListaRoles(List<UsuRol> listaRoles) {
		this.listaRoles = listaRoles;
	}

	public List<UsuUsuario> getListaUsuarios() {
		return listaUsuarios;
	}

	public void setListaUsuarios(List<UsuUsuario> listaUsuarios) {
		this.listaUsuarios = listaUsuarios;
	}

	public List<UsuUsuarioRol> getListaUsuariosRoles() {
		return listaUsuariosRoles;
	}

	public void setListaUsuariosRoles(List<UsuUsuarioRol> listaUsuariosRoles) {
		this.listaUsuariosRoles = listaUsuariosRoles;
	}

	public UsuUsuarioRol getUsurol() {
		return usurol;
	}

	public void setUsurol(UsuUsuarioRol usurol) {
		this.usurol = usurol;
	}

}
