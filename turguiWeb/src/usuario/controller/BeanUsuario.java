package usuario.controller;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import multimedia.controller.JSFUtil;
import turgui.model.dto.LoginDTO;
import turgui.model.entities.UsuUsuario;
import usuario.model.manager.ManagerAuditoria;
import usuario.model.manager.ManagerUsuario;

@Named
@SessionScoped
public class BeanUsuario implements Serializable {
	private static final long serialVersionUID = 1L;

	@EJB
	private ManagerUsuario managerUsuario;
	
	@EJB
	private ManagerAuditoria managerAuditoria;

	private UsuUsuario usuario;
	private List<UsuUsuario> listaUsuario;
	private int id_nacionalidad;

	@PostConstruct
	public void inicializar() {
		listaUsuario = managerUsuario.findAllUsuarios();
		usuario = new UsuUsuario();
	}

	public void actionInsertarUsuario() {
		try {
			listaUsuario = managerUsuario.findAllUsuarios();
			managerUsuario.insertarUsuario(usuario, id_nacionalidad);
			listaUsuario = managerUsuario.findAllUsuarios();
			
			//Para insertar en la tabla auditoria
			ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
			Map<String, Object> sessionMap = externalContext.getSessionMap();
			LoginDTO loginDTO = (LoginDTO) sessionMap.get("loginDTO");
			managerAuditoria.crearEventoBitacora(loginDTO.getCorreo(), this.getClass(), "Registro de usuario", "Registro de un nuevo usuario",
					"local_host");
			
			usuario = new UsuUsuario();
			JSFUtil.crearMensajeInfo("Se ha registrado un usuario");
			
			
		} catch (Exception e) {
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}
	
	
	public void actionRegistrarUsuario() {
		try {
			listaUsuario = managerUsuario.findAllUsuarios();
			managerUsuario.registrarUsuario(usuario, id_nacionalidad);
			listaUsuario = managerUsuario.findAllUsuarios();
		} catch (Exception e) {
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}
	
	
	
	public void actionEliminarUsuario() {
		try {
			listaUsuario = managerUsuario.findAllUsuarios();
			managerUsuario.eliminarUsuario(usuario.getIdUsuario());
			listaUsuario = managerUsuario.findAllUsuarios();
			
			//Para insertar en la tabla auditoria
			ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
			Map<String, Object> sessionMap = externalContext.getSessionMap();
			LoginDTO loginDTO = (LoginDTO) sessionMap.get("loginDTO");
			managerAuditoria.crearEventoBitacora(loginDTO.getCorreo(), this.getClass(), "Elimina usuario", "Eliminacion de un usuario",
					"local_host");
			
			usuario = new UsuUsuario();
			JSFUtil.crearMensajeInfo("Se ha eliminado un usuario");
		} catch (Exception e) {
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}
	
	
	public void actionActualizarUsuario() {
		try {
			listaUsuario = managerUsuario.findAllUsuarios();
			managerUsuario.actualizarUsuario(usuario);
			listaUsuario = managerUsuario.findAllUsuarios();
			usuario = new UsuUsuario();
			listaUsuario = managerUsuario.findAllUsuarios();
			JSFUtil.crearMensajeInfo("Se ha actualizado un usuario");
		} catch (Exception e) {
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}
	
	
	public void cambiarDatosUsuario(UsuUsuario usuario_nuevo) {
		usuario.setIdUsuario(usuario_nuevo.getIdUsuario());
		usuario.setNombres(usuario_nuevo.getNombres());
		usuario.setApellidos(usuario_nuevo.getApellidos());
		usuario.setCorreo(usuario_nuevo.getCorreo());
		usuario.setContrasenia(usuario_nuevo.getContrasenia());
		usuario.setFechaCreacion(usuario_nuevo.getFechaCreacion());
		usuario.setFechaModificacion(usuario_nuevo.getFechaModificacion());
		usuario.setConfirmacionCorreo(usuario_nuevo.getConfirmacionCorreo());
		listaUsuario = managerUsuario.findAllUsuarios();
	}

	public BeanUsuario() {
	}

	public UsuUsuario getUsuario() {
		return usuario;
	}

	public void setUsuario(UsuUsuario usuario) {
		this.usuario = usuario;
	}

	public List<UsuUsuario> getListaUsuario() {
		return listaUsuario;
	}

	public void setListaUsuario(List<UsuUsuario> listaUsuario) {
		this.listaUsuario = listaUsuario;
	}

	public int getId_nacionalidad() {
		return id_nacionalidad;
	}

	public void setId_nacionalidad(int id_nacionalidad) {
		this.id_nacionalidad = id_nacionalidad;
	}
	
	

}
