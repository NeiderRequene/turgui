package visitante.controller;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import lugar.model.manager.ManagerContacto;
import lugar.model.manager.ManagerLugar;
import turgui.model.dto.LugarDTO;

import java.io.Serializable;

@Named
@SessionScoped
public class BeanVisLugar implements Serializable {
	private static final long serialVersionUID = 1L;
	private LugarDTO lugar;
	private int id;
	@EJB
	private ManagerLugar managerLugar;
	@EJB
	private ManagerContacto managerContacto;

	
	public void inicializar() {

	}
	@PostConstruct
	public void buscador(int id) {
		this.id = id;
		lugar = managerLugar.buscarLugarDTOPorId(this.id);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public LugarDTO getLugar() {
		return lugar;
	}

	public void setLugar(LugarDTO lugar) {
		this.lugar = lugar;
	}

}
