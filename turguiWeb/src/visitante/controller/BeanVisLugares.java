package visitante.controller;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import lugar.model.manager.ManagerLugar;
import multimedia.model.manager.ManagerMultimedia;
import turgui.model.dto.LugarCardDTO;
import turgui.model.entities.LugLugar;
import turgui.model.entities.MulMultimedia;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Named
@SessionScoped
public class BeanVisLugares implements Serializable {
	private static final long serialVersionUID = 1L;
	private List<LugarCardDTO> listaLugares;
	private int idLugar;
	@EJB
	private ManagerLugar managerLugar;
	@EJB
	private ManagerMultimedia managerMultimedia;

	@PostConstruct
	public void inicializar() {
		setListaLugares(managerLugar.consultarLugaresCard());
	}
	
	public String actionLinkToLugares() {
		setListaLugares(managerLugar.consultarLugaresCard());
		return "lugares";
	}

	public MulMultimedia buscarImagenMultimedia(LugLugar lugar) {
		List<MulMultimedia> lista_multi = managerMultimedia.findMultimediaByIdLugar(lugar.getIdLugar());
		if (lista_multi.size() > 0) {
			return lista_multi.get(0);
		} else {
			return null;
		}

	}

	public List<LugarCardDTO> getListaLugares() {
		return listaLugares;
	}

	public void setListaLugares(List<LugarCardDTO> listaLugares) {
		this.listaLugares = listaLugares;
	}

	public int getIdLugar() {
		return idLugar;
	}

	public void setIdLugar(int idLugar) {
		this.idLugar = idLugar;
	}

}
